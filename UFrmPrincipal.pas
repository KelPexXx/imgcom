unit UFrmPrincipal;

interface

uses
  Windows, Messages, SysUtils,System.UITypes, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, rkView, ExtCtrls, StdCtrls, JvExStdCtrls, JvButton, JvCtrls,
  ImgList, PngImageList, Jpeg, ShellApi, Math, JvBaseDlg, JvBrowseFolder, IniFiles,
  Generics.Collections, pngimage,Registry, Menus, AppEvnts, JvComponentBase,
  JvSerialMaker, JvAppAnimatedIcon, JvFormAnimatedIcon, JvExComCtrls,UFrmVisualizando;

const
  CM_UpdateView = WM_USER + 2102; // Custom Message...

type
  TIntList = class(TList<Integer>);

  PCacheItem = ^TCacheItem;
  TCacheItem = record
    Idx: Integer;
    Size: Integer;
    Age: TDateTime;
    Scale: Integer;
    Bmp: TBitmap;
  end;

  PThumbData = ^TThumbData;
  TThumbData = record
    Name: string;
    ThumbWidth: Word;
    ThumbHeight: Word;
    Size: Integer;
    iCheck:boolean;
    Modified: TDateTime;
    IWidth, IHeight: Word;
    GotThumb: Boolean;
    Image: TObject;
  end;

  ThumbThread = class(TThread)
  private
    { Private declarations }
    XPView: TrkView;
    XPList: TList;
  protected
    procedure Execute; override;
  public
    constructor Create(xpThumbs: TrkView; Items: TList);
  end;
  TFrmPrincipal = class(TForm)
    pnlTop: TPanel;
    ImgListView: TrkView;
    BarraStatus: TStatusBar;
    pnlBotton: TPanel;
    lblTextTop: TLabel;
    lblInfo: TLabel;
    lblArquivo: TLabel;
    lateral: TPanel;
    SizerImg: TTrackBar;
    baixo: TPanel;
    lblLocalOrig: TLabel;
    lblTextBotton: TLabel;
    lblLocalDestDir: TLabel;
    lblNomePadrao: TLabel;
    edtNomePadrao: TEdit;
    lblSerie: TLabel;
    edtSerie: TEdit;
    botoesbaixo: TPanel;
    imgBtnNor: TPngImageList;
    BFFOrigem: TJvBrowseForFolderDialog;
    BFFDestino: TJvBrowseForFolderDialog;
    imgZoomMenos: TImage;
    imgZoomMais: TImage;
    pbCopia: TProgressBar;
    MenuPrincipal: TPopupMenu;
    mSelecionaOrigem: TMenuItem;
    mSelecionarDestino: TMenuItem;
    mExecutarCopia: TMenuItem;
    mSobre: TMenuItem;
    mFechar: TMenuItem;
    Autenticador: TJvSerialMaker;
    iconanimado: TPngImageList;
    animaIcone: TJvFormAnimatedIcon;
    btnBuscaOrigem: TButton;
    btnSelTudo: TButton;
    btnSelInvert: TButton;
    btnBuscaDestino: TButton;
    btnExecCopia: TButton;
    pMVisualiza: TMenuItem;
    btnVisualizar: TButton;
    procedure UpdateStatus;
    procedure BiResample(Src, Dest: TBitmap; Sharpen: Boolean);
    function ThumbImage(Filename: string): TBitmap;
    procedure OpenDir;
    procedure btnBuscaOrigemClick(Sender: TObject);
    procedure CMUpdateView(var message: TMessage); message CM_UpdateView;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
      procedure ItemPaintBasic(Canvas: TCanvas; R: TRect; State: TsvItemState);
      procedure GenCellColors;
      function Running: Boolean;
      procedure Start;
      procedure Stop;
    procedure ImgListViewCellPaint(Sender: TObject; Canvas: TCanvas;
      Cell: TRect; IdxA, Idx: Integer; State: TsvItemState);
    procedure ImgListViewSelecting(Sender: TObject; Count: Integer);
    procedure SizerImgChange(Sender: TObject);
    procedure ImgListViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnSelTudoClick(Sender: TObject);
    procedure btnSelInvertClick(Sender: TObject);
    procedure btnBuscaDestinoClick(Sender: TObject);
    procedure BarraStatusDrawPanel(StatusBar: TStatusBar; Panel: TStatusPanel;
      const Rect: TRect);
    procedure btnExecCopiaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure mFecharClick(Sender: TObject);
    procedure mSelecionaOrigemClick(Sender: TObject);
    procedure mSelecionarDestinoClick(Sender: TObject);
    procedure mExecutarCopiaClick(Sender: TObject);
    procedure mSobreClick(Sender: TObject);
    procedure pMVisualizaClick(Sender: TObject);
    procedure atualizaSelecionados(selecteds:TArray<UFrmVisualizando.Arquivos>);
    procedure btnVisualizarClick(Sender: TObject);
  private
    { Private declarations }
    Items: TList;
    ThumbSizeW, ThumbSizeH: Integer;

    ThumbJPEG: TJpegImage;

    procedure SetThumbSize(Value: Integer; UpdateTrackbar: Boolean);
    function ThumbBmp(idx: Integer): TBitmap;
    procedure ThumbsGetThumbnail(Sender: TObject; Thumb: PThumbData);
    procedure ClearThumbs;
    procedure ClearThumbsPool;
    //FCopy
    //Fun��es
    function CopiarArquivos(Origem, Destino, NomePadrao: string;  NumSerie: Integer; Gauge: TProgressBar; StatusBar : TStatusBar): Boolean;
    function VerificaTipo(nome: string): Boolean;
    function VerificaSelecionados(nome: string): Boolean;
    function ObtemSerie(Diretorio:String): integer;
    function ConvertHextoDec(codHex:string):integer;
    function VerificaArquivo(sArquivo:string):boolean;
    //procedimentos
    procedure AtualizaRegistro;
    procedure CarregaRegistro;
    procedure AjustarTamanho;
    procedure LibBloq(Status:boolean);

  protected
    ThumbThr: ThumbThread;
    ThreadDone: Boolean;
    hCodHD,sNome,sSerial,rNomePadrao,rSerie,rUltimoDestino:string;
  public
    { Public declarations }
    Directory: string;
    lastSelection: integer;
    // Colors
    cGSelectedStart,
      cGSelectedEnd,
      cGHotStart,
      cGHotEnd,
      cGDisabledStart,
      cGDisabledEnd,
      cGHeaderStart,
      cGHeaderEnd,
      cGHeaderHotStart,
      cGHeaderHotEnd,
      cGHeaderSelStart,
      cGHeaderSelEnd,
      cHot,
      cSelected,
      cDisabled,
      cBackground,
      cLineHighLight: TColor;
    cShadeSelect: TColor;
    cShadeDisabled: TColor;
    CellShade0: TColor;
    CellShade1: TColor;
    CellShade2: TColor;
    CellShade3: TColor;
    CellShade4: TColor;
    CellBkgColor: TColor;
    CellBrdColor: array[Boolean, Boolean] of TColor;
  end;
var
  FrmPrincipal: TFrmPrincipal;
  CellJpeg: TJpegImage;
  WI, HI, TW, TH, HSX, vIdx: Integer;
  CellScale: Integer;
  CellStyle: Integer;
  ThumbsPool: TList;
  PoolSize, MaxPool: Integer;

implementation

uses UFrmSobre, UFrmTerminado, UFrmAtivacao, UFrmVersaoteste;

{$R *.dfm}

{Este Programa foi desenvolvido por Rafael Werner, com base em conhecimentos proprios e
de conteudos retirados de forums.}
function BytesToStr(const i64Size: Int64): string;
const
  i64GB = 1024 * 1024 * 1024;
  i64MB = 1024 * 1024;
  i64KB = 1024;
begin
  if i64Size div i64GB > 0 then
    Result := Format('%.1f GB', [i64Size / i64GB])
  else if i64Size div i64MB > 0 then
    Result := Format('%.2f MB', [i64Size / i64MB])
  else if i64Size div i64KB > 0 then
    Result := Format('%.0f kB', [i64Size / i64KB])
  else
    Result := IntToStr(i64Size) + ' byte';
end;


function ErroPersonalizado(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; Captions: array of string;
  FormCaption : string = ''): Integer;
var
  aMsgDlg: TForm;
  I: Integer;
  DlgButton: TButton;
  CaptionIndex: Integer;
begin
  aMsgDlg := CreateMessageDialog(Msg, DlgType, Buttons);
  CaptionIndex := 0;
  if Trim(FormCaption) <> '' then
    aMsgDlg.Caption := FormCaption;
  for I := 0 to aMsgDlg.ComponentCount - 1 do
  begin
    if (aMsgDlg.Components[i] is TButton) then
    begin
      dlgButton := TButton(aMsgDlg.Components[i]);
      if CaptionIndex > High(Captions) then
        Break;
      dlgButton.Caption := Captions[CaptionIndex];
      Inc(CaptionIndex);
    end;
  end;
  Result := aMsgDlg.ShowModal;
end;

procedure TFrmPrincipal.UpdateStatus;
var
  i: integer;
  n: Int64;
  item: PThumbData;
begin
  n := 0;
  for i := 0 to ImgListView.Selection.Count - 1 do
  begin
    item := Items[ImgListView.Selection[i]];
    n := n + item.Size;
  end;
  lblInfo.Caption := IntToStr(ImgListView.Items.Count) + ' itens, ' +
    IntToStr(ImgListView.Selection.Count) + ' selecionados (' + BytesToStr(n) + ')';
    if ImgListView.Selection.Count > 0 then
      lastSelection := ImgListView.Selection[ImgListView.Selection.Count-1]
    else
      lastSelection := -1;
end;

function TFrmPrincipal.VerificaArquivo(sArquivo: string): boolean;
var
  Rec: TSearchRec;
  Res: Integer;
begin
  Result := False;
  try
    Res := FindFirst(sArquivo, faAnyFile, Rec);
    while Res = 0 do
      begin
        Application.ProcessMessages;
        if (Rec.Attr = faArchive)  then
          begin
            result := True;
          end;
        Res := FindNext(Rec);
      end;
    FindClose(Rec);
  except
    on E: Exception do
      Showmessage('Erro na busca pelos arquivos');
  end;
end;

function TFrmPrincipal.VerificaSelecionados(nome: string): Boolean;
var
  I : integer;
  item: PThumbData;
begin
  Result := False;
  for I := 0 to ImgListView.Selection.Count - 1 do
    begin
      item := Items[ImgListView.Selection[i]];
      if nome = item.Name then
        Result := True;
    end;
end;

function TFrmPrincipal.VerificaTipo(nome: string): Boolean;
begin
  if (nome <> '.') and (nome <> '..') and (
      ((UpperCase(ExtractFileExt(nome)) = '.JPEG')) OR
      ((UpperCase(ExtractFileExt(nome)) = '.JPG')) ) then
  begin
    result := True;
  end
  else
    result := False;
end;

function ReadMWord(f: TFileStream): Word;
type
  TMotorolaWord = record
    case Byte of
      0: (Value: Word);
      1: (Byte1, Byte2: Byte);
  end;
var
  MW: TMotorolaWord;
begin
  f.read(MW.Byte2, SizeOf(Byte));
  f.read(MW.Byte1, SizeOf(Byte));
  Result := MW.Value;
end;

procedure GetJPGSize(const sFile: string; var wWidth, wHeight: Integer);
const
  ValidSig: array[0..1] of Byte = ($FF, $D8);
  Parameterless = [$01, $D0, $D1, $D2, $D3, $D4, $D5, $D6, $D7];
var
  Sig: array[0..1] of byte;
  f: TFileStream;
  x: integer;
  Seg: byte;
  Dummy: array[0..15] of byte;
  Len: word;
  ReadLen: LongInt;
begin
  FillChar(Sig, SizeOf(Sig), #0);
  f := TFileStream.Create(sFile, fmOpenRead);
  try
    ReadLen := f.read(Sig[0], SizeOf(Sig));
    for x := Low(Sig) to High(Sig) do
      if Sig[x] <> ValidSig[x] then
        ReadLen := 0;
    if ReadLen > 0 then
    begin
      ReadLen := f.read(Seg, 1);
      while (Seg = $FF) and (ReadLen > 0) do
      begin
        ReadLen := f.read(Seg, 1);
        if Seg <> $FF then
        begin
          if (Seg = $C0) or (Seg = $C1) then
          begin
            ReadLen := f.read(Dummy[0], 3);
            wHeight := ReadMWord(f);
            wWidth := ReadMWord(f);
          end
          else
          begin
            if not (Seg in Parameterless) then
            begin
              Len := ReadMWord(f);
              f.Seek(Len - 2, 1);
              f.read(Seg, 1);
            end
            else
              Seg := $FF;
          end;
        end;
      end;
    end;
  finally
    f.Free;
  end;
end;

function CalcThumbSize(w, h, tw, th: integer): TPoint;
begin
  Result.X := 0;
  Result.Y := 0;
  if (w < tw) and (h < th) then
  begin
    Result.X := w;
    Result.Y := h;
  end
  else if (w = 0) or (h = 0) then
    Exit
  else
  begin
    if w > h then
    begin
      if w < tw then
        tw := w;
      Result.X := tw;
      Result.Y := Trunc(tw * h / w);
      if Result.Y > th then
      begin
        Result.Y := th;
        Result.X := Trunc(th * w / h);
      end;
    end
    else
    begin
      if h < th then
        th := h;
      Result.Y := th;
      Result.X := Trunc(th * w / h);
      if Result.X > tw then
      begin
        Result.X := tw;
        Result.Y := Trunc(tw * h / w);
      end;
    end;
  end;
end;

procedure MakeThumbNail(Src, Dst: TBitmap);
var
  x, y, ix, iy, w, h, dx, dy: Integer;
  x1, x2, x3: integer;
  RowDest, RowSource, RowSourceStart: Integer;
  iRatio: Integer;
  Ratio: Single;
  iRed, iGrn, iBlu: Integer;
  pt: PRGB24;
  iSrc, iDst: Integer;
  lutW, lutH: array of Integer;
begin
  if (Src.Width <= Dst.Width) and (Src.Height <= Dst.Height) then
  begin
    Dst.Assign(Src);
    Exit;
  end;
  w := Dst.Width;
  h := Dst.Height;
  Ratio := 1 / (w / Src.Width);
  SetLength(lutW, w);
  x1 := 0;
  x2 := Trunc(Ratio);
  for x := 0 to w - 1 do
  begin
    lutW[x] := x2 - x1;
    x1 := x2;
    x2 := Trunc((x + 2) * Ratio);
  end;
  Ratio := 1 / (h / Src.Height);
  SetLength(lutH, h);
  x1 := 0;
  x2 := Trunc(Ratio);
  for x := 0 to h - 1 do
  begin
    lutH[x] := x2 - x1;
    x1 := x2;
    x2 := Trunc((x + 2) * Ratio);
  end;
  RowDest := Integer(Dst.Scanline[0]);
  RowSourceStart := integer(Src.Scanline[0]);
  RowSource := RowSourceStart;
  iDst := ((w * 24 + 31) and not 31) shr 3;
  iSrc := ((Src.Width * 24 + 31) and not 31) shr 3;
  for y := 0 to h - 1 do
  begin
    dy := lutH[y];
    x1 := 0;
    x3 := 0;
    for x := 0 to w - 1 do
    begin
      dx := lutW[x];
      iRed := 0;
      iGrn := 0;
      iBlu := 0;
      RowSource := RowSourceStart;
      for iy := 1 to dy do
      begin
        pt := PRGB24(RowSource + x1);
        for ix := 1 to dx do
        begin
          iRed := iRed + pt.R;
          iGrn := iGrn + pt.G;
          iBlu := iBlu + pt.B;
          inc(pt);
        end;
        RowSource := RowSource - iSrc;
      end;
      iRatio := $00FFFFFF div (dx * dy);
      pt := PRGB24(RowDest + x3);
      pt.R := (iRed * iRatio) shr 24;
      pt.G := (iGrn * iRatio) shr 24;
      pt.B := (iBlu * iRatio) shr 24;
      x1 := x1 + 3 * dx;
      inc(x3, 3);
    end;
    RowDest := RowDest - iDst;
    RowSourceStart := RowSource;
  end;
end;

procedure TFrmPrincipal.AjustarTamanho;
var
  tm : integer;
begin
  tm := FrmPrincipal.Width div 5;
  BarraStatus.Panels[0].Width := tm;
  BarraStatus.Panels[2].Width := tm;
  BarraStatus.Panels[1].Width := tm * 3;
end;

procedure TFrmPrincipal.AtualizaRegistro;
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\WestSoftwares\ImgCom',False);
  padrao.WriteString('NomePadrao',edtNomePadrao.Text);
  padrao.WriteInteger('NumSerie',StrToInt(edtSerie.Text));
  padrao.WriteString('UltimoDestino',lblLocalDestDir.Caption);
  padrao.CloseKey();
  padrao.Free;
end;

procedure TFrmPrincipal.atualizaSelecionados(selecteds:TArray<UFrmVisualizando.Arquivos>);
var
  I: Integer;
begin
    ImgListView.Selection.Clear;
    for I := 0 to Length(selecteds)-1 do
      begin
        if selecteds[i].Selecionado then
          ImgListView.Selection.Add(i);
      end;
    ImgListView.UpdateView;
end;

procedure TFrmPrincipal.BarraStatusDrawPanel(StatusBar: TStatusBar;
  Panel: TStatusPanel; const Rect: TRect);
begin
  if Panel.Index = 1 then
    begin
    pbCopia.Width := Rect.Right - Rect.Left +1;
    pbCopia.Height := Rect.Bottom - Rect.Top +1;
    pbCopia.PaintTo(StatusBar.Canvas.Handle, Rect.Left, Rect.Top);
  end;
end;

procedure TFrmPrincipal.BiResample(Src, Dest: TBitmap; Sharpen: Boolean);
type
  PRGB24 = ^TRGB24;
  TRGB24 = record B, G, R: Byte;
  end;
  PRGBArray = ^TRGBArray;
  TRGBArray = array[0..0] of TRGB24;
var
  x, y, px, py: Integer;
  i, x1, x2, z, z2, iz2: Integer;
  w1, w2, w3, w4: Integer;
  Ratio: Integer;
  sDst, sDstOff: Integer;
  sScanLine: array[0..255] of PRGBArray;
  Src1, Src2: PRGBArray;
  C, C1, C2: TRGB24;
  y1, y2, y3, x3, iRed, iGrn, iBlu: Integer;
  p1, p2, p3, p4, p5: PRGB24;
begin
  sDst := Integer(src.Scanline[0]);
  sDstOff := Integer(src.Scanline[1]) - sDst;
  for i := 0 to src.Height - 1 do
  begin
    sScanLine[i] := PRGBArray(sDst);
    sDst := sDst + sDstOff;
  end;
  sDst := Integer(Dest.Scanline[0]);
  y1 := sDst;
  sDstOff := Integer(Dest.Scanline[1]) - sDst;
  Ratio := ((src.Width - 1) shl 15) div Dest.Width;
  py := 0;
  for y := 0 to Dest.Height - 1 do
  begin
    i := py shr 15;
    if i > src.Height - 1 then
      i := src.Height - 1;
    Src1 := sScanLine[i];
    if i < src.Height - 1 then
      Src2 := sScanLine[i + 1]
    else
      Src2 := Src1;
    z2 := py and $7FFF;
    iz2 := $8000 - z2;
    px := 0;
    for x := 0 to Dest.Width - 1 do
    begin
      x1 := px shr 15;
      x2 := x1 + 1;
      C1 := Src1[x1];
      C2 := Src2[x1];
      z := px and $7FFF;
      w2 := (z * iz2) shr 15;
      w1 := iz2 - w2;
      w4 := (z * z2) shr 15;
      w3 := z2 - w4;
      C.R := (C1.R * w1 + Src1[x2].R * w2 + C2.R * w3 + Src2[x2].R * w4) shr 15;
      C.G := (C1.G * w1 + Src1[x2].G * w2 + C2.G * w3 + Src2[x2].G * w4) shr 15;
      C.B := (C1.B * w1 + Src2[x2].B * w2 + C2.B * w3 + Src2[x2].B * w4) shr 15;
      PRGBArray(sDst)[x] := C;
      inc(px, Ratio);
    end;
    sDst := sDst + sDstOff;
    inc(py, Ratio);
  end;

  Exit;

  y2 := y1 + sDstOff;
  y3 := y2 + sDstOff;
  for y := 1 to Dest.Height - 2 do
  begin
    for x := 0 to Dest.Width - 3 do
    begin
      x1 := x * 3;
      x2 := x1 + 3;
      x3 := x1 + 6;
      p1 := PRGB24(y1 + x1);
      p2 := PRGB24(y1 + x3);
      p3 := PRGB24(y2 + x2);
      p4 := PRGB24(y3 + x1);
      p5 := PRGB24(y3 + x3);
      // -15 -11                       // -17 - 13
      iRed := (p1.R + p2.R + (p3.R * -15) + p4.R + p5.R) div -11;
      iGrn := (p1.G + p2.G + (p3.G * -15) + p4.G + p5.G) div -11;
      iBlu := (p1.B + p2.B + (p3.B * -15) + p4.B + p5.B) div -11;
      if iRed < 0 then
        iRed := 0
      else if iRed > 255 then
        iRed := 255;
      if iGrn < 0 then
        iGrn := 0
      else if iGrn > 255 then
        iGrn := 255;
      if iBlu < 0 then
        iBlu := 0
      else if iBlu > 255 then
        iBlu := 255;
      PRGB24(y2 + x2).R := iRed;
      PRGB24(y2 + x2).G := iGrn;
      PRGB24(y2 + x2).B := iBlu;
    end;
    inc(y1, sDstOff);
    inc(y2, sDstOff);
    inc(y3, sDstOff);
  end;
end;

function TFrmPrincipal.ThumbImage(Filename: string): TBitmap;
var
  s: string;
  tmp: TBitmap;
  ThumbSize: TPoint;
  bmp: TBitmap;
  newW, newH: Integer;
  sf: Integer;
  fail: Boolean;
begin
  Result := nil;
  tmp := nil;
  bmp := TBitmap.Create;
  bmp.Canvas.Lock;
  s := Filename;
  if Filename <> '' then
  begin
    ThumbJpeg.Scale := jsFullSize;
    GetJPGSize(Filename, WI, HI);
    if WI < 1 then
      WI := 1;
    if HI < 1 then
      HI := 1;
    sf := Trunc(Min(HI / 255 {TH}, WI / 255 {TW}));
    if sf < 0 then
      sf := 0;
    case sf of
      0..1: ThumbJpeg.Scale := jsFullSize;
      2..3: ThumbJpeg.Scale := jsHalf;
      4..7: ThumbJpeg.Scale := jsQuarter;
    else
      ThumbJpeg.Scale := jsEighth;
    end;
    fail := False;
    try
      ThumbJpeg.LoadFromFile(Filename);
    except
      ShowMessage('Ocorreu um erro ao abrir o arquivo: ' + Filename);
      fail := True;
    end;
    if not fail then
    begin
      bmp.PixelFormat := pf24Bit;
      bmp.Width := ThumbJpeg.Width;
      bmp.Height := ThumbJpeg.Height;
      bmp.Canvas.Draw(0, 0, ThumbJpeg);
      try
        tmp := TBitmap.Create;
        tmp.Canvas.Lock;
        ThumbSize := CalcThumbSize(bmp.Width, bmp.Height, ThumbSizeW,
          ThumbSizeH);
        newW := ThumbSize.X;
        newH := ThumbSize.Y;
        if newW <= 0 then
          newW := 1;
        if newH <= 0 then
          newH := 1;
        tmp.PixelFormat := pf24Bit;
        tmp.Width := newW;
        tmp.Height := newH;
        MakeThumbNail(bmp, tmp);
        tmp.Canvas.UnLock;
      except
        tmp.Free;
        raise;
      end;
      Result := tmp;
    end
    else
      Result := nil;
  end;
  bmp.Canvas.UnLock;
  bmp.Free;
end;

procedure TFrmPrincipal.ThumbsGetThumbnail(Sender: TObject; Thumb: PThumbData);
var
  FName: string;
  ThumbBmp, bmp: TBitmap;
  MS: TMemoryStream;
  ThumbSize: TPoint;
  newW, newH: Integer;
  sf: Integer;
  fail: Boolean;
begin
  FName := Directory + Thumb.name;
  ThumbBmp := nil;
  bmp := TBitmap.Create;
  if FName <> '' then
  begin
    ThumbJpeg.Scale := jsFullSize;
    GetJPGSize(FName, WI, HI);
    if WI < 1 then
      WI := 1;
    if HI < 1 then
      HI := 1;
    sf := Trunc(Min(HI / 255 {TH}, WI / 255 {TW}));
    if sf < 0 then
      sf := 0;
    case sf of
      0..1: ThumbJpeg.Scale := jsFullSize;
      2..3: ThumbJpeg.Scale := jsHalf;
      4..7: ThumbJpeg.Scale := jsQuarter;
    else
      ThumbJpeg.Scale := jsEighth;
    end;
    fail := False;
    try
      ThumbJpeg.LoadFromFile(FName);
    except
      ShowMessage('Ocorreu um erro ao abrir o arquivo: ' + FName);
      fail := True;
    end;
    if not fail then
    begin
      bmp.PixelFormat := pf24Bit;
      bmp.Width := ThumbJpeg.Width;
      bmp.Height := ThumbJpeg.Height;
      bmp.Canvas.Lock;
      bmp.Canvas.Draw(0, 0, ThumbJpeg);
      try
        ThumbBmp := TBitmap.Create;
        ThumbBmp.Canvas.Lock;
        ThumbSize := CalcThumbSize(bmp.Width, bmp.Height, ThumbSizeW,
          ThumbSizeH);
        newW := ThumbSize.X;
        newH := ThumbSize.Y;
        if newW <= 0 then
          newW := 1;
        if newH <= 0 then
          newH := 1;
        ThumbBmp.PixelFormat := pf24Bit;
        ThumbBmp.Width := newW;
        ThumbBmp.Height := newH;
        MakeThumbNail(bmp, ThumbBmp);
      except
        ThumbBmp.Canvas.UnLock;
        FreeAndNil(ThumbBmp);
        raise;
      end;
    end
  end;

  bmp.Canvas.UnLock;
  bmp.Free;

  if ThumbBmp <> nil then
  begin
    ThumbJpeg.Assign(ThumbBmp);
    ThumbJpeg.Compress;
    MS := TMemoryStream.Create;
    MS.Position := 0;
    try
      ThumbJpeg.SaveToStream(MS);
    except
      MS.Free;
      raise;
    end;
    Thumb.Image := MS;
    Thumb.ThumbWidth := ThumbBmp.Width;
    Thumb.ThumbHeight := ThumbBmp.Height;
    ThumbBmp.Canvas.UnLock;
    ThumbBmp.Free;
  end
  else
    Thumb.Image := nil;
  Thumb.GotThumb := True;
end;

function Blend(Color1, Color2: TColor; A: Byte): TColor;
var
  c1, c2: LongInt;
  r, g, b, v1, v2: byte;
begin
  A := Round(2.55 * A);
  c1 := ColorToRGB(Color1);
  c2 := ColorToRGB(Color2);
  v1 := Byte(c1);
  v2 := Byte(c2);
  r := A * (v1 - v2) shr 8 + v2;
  v1 := Byte(c1 shr 8);
  v2 := Byte(c2 shr 8);
  g := A * (v1 - v2) shr 8 + v2;
  v1 := Byte(c1 shr 16);
  v2 := Byte(c2 shr 16);
  b := A * (v1 - v2) shr 8 + v2;
  Result := (b shl 16) + (g shl 8) + r;
end;

procedure TFrmPrincipal.GenCellColors;
begin
  cHot := $00FDDE99;
  cgHotStart := $00FDF5E6;
  cGHotEnd := $00FDFBF6;
  cSelected := $00FDCE99;
  cGSelectedStart := $00FCEFC4;
  cGSelectedEnd := $00FDF8EF;
  cShadeSelect := $00F8F3EA;
  cDisabled := $00D9D9D9;
  cGDisabledStart := $00EAE9E9;
  cGDisabledEnd := $00FCFBFB;
  cShadeDisabled := $00F6F5F5;
  cGHeaderStart := $00F9F9F9;
  cGHeaderEnd := $00FEFEFE;
  cGHeaderHotStart := $00FFEDBD;
  cGHeaderHotEnd := $00FFF7E3;
  cGHeaderSelStart := $00FCEABA;
  cGHeaderSelEnd := $00FCF4E0;
  cBackground := clWindow;
  ;
  cLineHighLight := $00FEFBF6;
  CellBkgColor := clWindow;
  CellBrdColor[False, False] := cDisabled;
  CellBrdColor[False, True] := cDisabled;
  CellBrdColor[True, False] := $00B5B5B5;
  CellBrdColor[True, True] := cSelected;
end;

procedure WinGradient( DC: HDC; ARect: TRect; AColor2, AColor1: TColor );
var
  Vertexs: array [ 0 .. 1 ] of TTriVertex;
  GRect: TGradientRect;
begin
  Vertexs[ 0 ].x := ARect.Left;
  Vertexs[ 0 ].y := ARect.Top;
  Vertexs[ 0 ].Red := ( AColor1 and $000000FF ) shl 8;
  Vertexs[ 0 ].Green := ( AColor1 and $0000FF00 );
  Vertexs[ 0 ].Blue := ( AColor1 and $00FF0000 ) shr 8;
  Vertexs[ 0 ].alpha := 0;
  Vertexs[ 1 ].x := ARect.Right;
  Vertexs[ 1 ].y := ARect.Bottom;
  Vertexs[ 1 ].Red := ( AColor2 and $000000FF ) shl 8;
  Vertexs[ 1 ].Green := ( AColor2 and $0000FF00 );
  Vertexs[ 1 ].Blue := ( AColor2 and $00FF0000 ) shr 8;
  Vertexs[ 1 ].alpha := 0;
  GRect.UpperLeft := 0;
  GRect.LowerRight := 1;
  GradientFill( DC, @Vertexs, 2, @GRect, 1, GRADIENT_FILL_RECT_V );
end;

procedure TFrmPrincipal.ItemPaintBasic(Canvas: TCanvas; R: TRect;
  State: TsvItemState);
var
  C: TColor;
begin
  Canvas.Brush.Style := bsClear;
  if ( State = svSelected ) or ( State = svHot ) then
  begin
    if ( ImgListView.Focused) and ( State = svSelected ) then
    begin
      Canvas.Pen.Color := cSelected;
      WinGradient( Canvas.Handle, R, cGSelectedStart, cGSelectedEnd );
    end
    else if ( State = svHot ) then
    begin
      Canvas.Pen.Color := cHot;
      WinGradient( Canvas.Handle, R, cGHotStart, cGHotEnd );
    end
    else
    begin
      Canvas.Pen.Color := cDisabled;
      WinGradient( Canvas.Handle, R, cGDisabledStart, cGDisabledEnd );
    end;
    Canvas.Rectangle( R );
    if ( ImgListView.Focused ) then
      C := cShadeSelect
    else
      C := cShadeDisabled;
    Canvas.Pen.Color := C;
    Canvas.MoveTo( R.Left + 1, R.Top + 2 );
    Canvas.LineTo( R.Left + 1, R.Bottom - 2 );
    Canvas.LineTo( R.Right - 2, R.Bottom - 2 );
    Canvas.LineTo( R.Right - 2, R.Top + 1 );
    Canvas.Pen.Style := psSolid;
    Canvas.Pixels[ R.Left, R.Top ] := C;
    Canvas.Pixels[ R.Left, R.Bottom - 1 ] := C;
    Canvas.Pixels[ R.Right - 1, R.Top ] := C;
    Canvas.Pixels[ R.Right - 1, R.Bottom - 1 ] := C;
  end;
end;

procedure TFrmPrincipal.LibBloq(Status: boolean);
begin
  btnBuscaOrigem.Enabled := Status;
  btnSelTudo.Enabled := Status;
  btnBuscaDestino.Enabled := Status;
  btnExecCopia.Enabled := Status;
  btnSelInvert.Enabled := Status;
  edtNomePadrao.Enabled := Status;
  edtSerie.Enabled := Status;
  ImgListView.Enabled := Status;
  mSelecionaOrigem.Enabled := Status;
  mSelecionarDestino.Enabled := Status;
  mExecutarCopia.Enabled := Status;
  mSobre.Enabled := Status;
  mFechar.Enabled := Status;
end;

procedure TFrmPrincipal.mSelecionarDestinoClick(Sender: TObject);
begin
  btnBuscaDestinoClick(sender);
end;

procedure TFrmPrincipal.mSelecionaOrigemClick(Sender: TObject);
begin
  btnBuscaOrigemClick(sender);
end;

procedure TFrmPrincipal.SetThumbSize(Value: Integer; UpdateTrackbar: Boolean);
var
  w, h: Integer;
begin
  case Value of
    32..63: CellJpeg.Scale := jsQuarter;
    64..127: CellJpeg.Scale := jsHalf;
    128..255: CellJpeg.Scale := jsFullSize;
  else
    CellJpeg.Scale := jsEighth;
  end;
  w := Value;
  h := Value;
  w := w + 20;
  if CellStyle = 0 then
    h := h + 20
  else
    h := h + 40;
  HSX := (w - 70) shr 1;
  ImgListView.CellWidth := w;
  ImgListView.CellHeight := h;
  CellScale := Value;
  if UpdateTrackbar then
  begin
    SizerImg.OnChange := nil;
    SizerImg.Position := CellScale;
    SizerImg.OnChange := SizerImgChange;
  end;
  ImgListView.CalcView(False);
  if not UpdateTrackBar then
    ImgListView.SetAtTop(-1, vIdx);
end;

procedure TFrmPrincipal.Start;
begin
  if Running then
    Exit;
  ThreadDone := False;
  ThumbThr := ThumbThread.Create(ImgListView, Items);
end;

procedure TFrmPrincipal.Stop;
begin
  if ThumbThr <> nil then
  begin
    ThumbThr.Terminate;
    ThumbThr.WaitFor;
    ThumbThr.Free;
    ThumbThr := nil;
  end;
end;

procedure TFrmPrincipal.SizerImgChange(Sender: TObject);
begin
  SetThumbSize(SizerImg.Position, False);
end;

procedure TFrmPrincipal.mSobreClick(Sender: TObject);
begin
  FrmSobre := TFrmSobre.Create(Self);
  FrmSobre.Show;
end;

procedure BiReSample(src, Dest: TBitmap; Sharpen: Boolean);
type
  TRGB24 = record
    B, G, R: Byte;
  end;
  PRGBArray = ^TRGBArray;
  TRGBArray = array[0..0] of TRGB24;
var
  x, y, px, py: Integer;
  i, x1, x2, z, z2, iz2: Integer;
  w1, w2, w3, w4: Integer;
  Ratio: Integer;
  sDst, sDstOff: Integer;
  sScanLine: array[0..255] of PRGBArray;
  Src1, Src2: PRGBArray;
  C, C1, C2: TRGB24;
  y1, y2, y3, x3, iRed, iGrn, iBlu: Integer;
  p1, p2, p3, p4, p5: PRGB24;
begin
  sDst := Integer(src.Scanline[0]);
  sDstOff := Integer(src.Scanline[1]) - sDst;
  for i := 0 to src.Height - 1 do
  begin
    sScanLine[i] := PRGBArray(sDst);
    sDst := sDst + sDstOff;
  end;
  sDst := Integer(Dest.Scanline[0]);
  y1 := sDst;
  sDstOff := Integer(Dest.Scanline[1]) - sDst;
  Ratio := ((src.Width - 1) shl 15) div Dest.Width;
  py := 0;
  for y := 0 to Dest.Height - 1 do
  begin
    i := py shr 15;
    if i > src.Height - 1 then
      i := src.Height - 1;
    Src1 := sScanLine[i];
    if i < src.Height - 1 then
      Src2 := sScanLine[i + 1]
    else
      Src2 := Src1;
    z2 := py and $7FFF;
    iz2 := $8000 - z2;
    px := 0;
    for x := 0 to Dest.Width - 1 do
    begin
      x1 := px shr 15;
      x2 := x1 + 1;
      C1 := Src1[x1];
      C2 := Src2[x1];
      z := px and $7FFF;
      w2 := (z * iz2) shr 15;
      w1 := iz2 - w2;
      w4 := (z * z2) shr 15;
      w3 := z2 - w4;
      C.R := (C1.R * w1 + Src1[x2].R * w2 + C2.R * w3 + Src2[x2].R * w4) shr 15;
      C.G := (C1.G * w1 + Src1[x2].G * w2 + C2.G * w3 + Src2[x2].G * w4) shr 15;
      C.B := (C1.B * w1 + Src2[x2].B * w2 + C2.B * w3 + Src2[x2].B * w4) shr 15;
      PRGBArray(sDst)[x] := C;
      inc(px, Ratio);
    end;
    sDst := sDst + sDstOff;
    inc(py, Ratio);
  end;

  if not Sharpen then
    Exit;

  y2 := y1 + sDstOff;
  y3 := y2 + sDstOff;
  for y := 1 to Dest.Height - 2 do
  begin
    for x := 0 to Dest.Width - 3 do
    begin
      x1 := x * 3;
      x2 := x1 + 3;
      x3 := x1 + 6;
      p1 := PRGB24(y1 + x1);
      p2 := PRGB24(y1 + x3);
      p3 := PRGB24(y2 + x2);
      p4 := PRGB24(y3 + x1);
      p5 := PRGB24(y3 + x3);
      // -15 -11                       // -17 - 13
      iRed := (p1.R + p2.R + (p3.R * -15) + p4.R + p5.R) div -11;
      iGrn := (p1.G + p2.G + (p3.G * -15) + p4.G + p5.G) div -11;
      iBlu := (p1.B + p2.B + (p3.B * -15) + p4.B + p5.B) div -11;
      if iRed < 0 then
        iRed := 0
      else if iRed > 255 then
        iRed := 255;
      if iGrn < 0 then
        iGrn := 0
      else if iGrn > 255 then
        iGrn := 255;
      if iBlu < 0 then
        iBlu := 0
      else if iBlu > 255 then
        iBlu := 255;
      PRGB24(y2 + x2).R := iRed;
      PRGB24(y2 + x2).G := iGrn;
      PRGB24(y2 + x2).B := iBlu;
    end;
    inc(y1, sDstOff);
    inc(y2, sDstOff);
    inc(y3, sDstOff);
  end;
end;

function TFrmPrincipal.Running: Boolean;
begin
  Result := ThumbThr <> nil;
end;

function TFrmPrincipal.ThumbBmp(idx: Integer): TBitmap;
var
  i, n, sf: Integer;
  p: PCacheItem;
  T: PThumbData;
  Bmp, tmp: TBitmap;
  pt: TPoint;
  Oldest: TDateTime;
begin
  Result := nil;
  if ThumbsPool.Count > 0 then
  begin
    i := ThumbsPool.Count - 1;
    while (i >= 0) and (PCacheItem(ThumbsPool[i]).Idx <> Idx) do
      i := i - 1;
    if i <> -1 then
    begin
      p := ThumbsPool[i];
      if (p.Idx = Idx) then
      begin
        if (p.Scale = CellScale) then
        begin
          p.Age := Now;
          Result := p.Bmp
        end
        else
        begin
          PoolSize := PoolSize - p.Size;
          p.Bmp.Free;
          Dispose(p);
          ThumbsPool.Delete(i);
        end;
      end;
    end;
  end;
  if Result = nil then
  begin
    T := Items[Idx];
    if T.Image <> nil then
    begin
      TMemoryStream(T.Image).Position := 0;

      sf := Trunc(Min(T.ThumbWidth / CellScale, T.ThumbHeight / CellScale));
      if sf < 0 then
        sf := 0;
      case sf of
        0..1: CellJPEG.Scale := jsFullSize;
        2..3: CellJPEG.Scale := jsHalf;
        4..7: CellJPEG.Scale := jsQuarter;
      else
        CellJPEG.Scale := jsEighth;
      end;
      CellJPEG.LoadFromStream(TMemoryStream(T.Image));

      Bmp := TBitmap.Create;
      Bmp.PixelFormat := pf24bit;
      pt := CalcThumbSize(CellJPEG.Width, CellJPEG.Height, CellScale,
        CellScale);
      if pt.x <> CellJPEG.Width then
      begin
        tmp := TBitmap.Create;
        tmp.PixelFormat := pf24bit;
        tmp.Width := CellJPEG.Width;
        tmp.Height := CellJPEG.Height;
        tmp.Canvas.Draw(0, 0, CellJPEG);
        Bmp.Width := pt.x;
        Bmp.Height := pt.y;
        if (Bmp.Width > 4) and (Bmp.Height > 4) then
          BiReSample(tmp, Bmp, False)
        else
          bmp.Canvas.StretchDraw(Rect(0, 0, pt.X, pt.Y), tmp);
        tmp.Free;
      end
      else
      begin
        Bmp.Width := CellJPEG.Width;
        Bmp.Height := CellJPEG.Height;
        Bmp.Canvas.Draw(0, 0, CellJPEG);
      end;
      New(p);
      p.Idx := Idx;
      p.Size := (Bmp.Width * Bmp.Height) * 3;
      p.Age := Now;
      p.Scale := CellScale;
      p.Bmp := Bmp;
      ThumbsPool.Add(p);
      PoolSize := PoolSize + p.Size;
      Result := p.Bmp;
      while (PoolSize > MaxPool) and (ThumbsPool.Count > 0) do
      begin
        Oldest := Now;
        n := 0;
        for i := 0 to ThumbsPool.Count - 1 do
        begin
          p := ThumbsPool[i];
          if p.Age <= Oldest then
          begin
            Oldest := p.Age;
            n := i;
          end;
        end;
        Assert(n >= 0);
        p := ThumbsPool[n];
        PoolSize := PoolSize - p.Size;
        p.Bmp.Free;
        Dispose(p);
        ThumbsPool.Delete(n);
      end;
    end;
  end;
end;

procedure TFrmPrincipal.ImgListViewCellPaint(Sender: TObject; Canvas: TCanvas;
  Cell: TRect; IdxA, Idx: Integer; State: TsvItemState);
var
  x, y: Integer;
  F, S: Boolean;
  R: TRect;
  TW, TH: Integer;
  Txt: string;
  Thumb: PThumbData;
  pt: TPoint;
begin
  Thumb := PThumbData(Items[idx]);
  pt := CalcThumbSize(Thumb.ThumbWidth, Thumb.ThumbHeight, CellScale,
    CellScale);
  TW := pt.X;
  TH := pt.Y;
  ItemPaintBasic(Canvas, Cell, State);
  F := ImgListView.Focused;
  S := State = svSelected;
  x := Cell.Left + ((Cell.Right - (Cell.Left + TW)) shr 1);
  y := Cell.Top + ((Cell.Bottom - (Cell.Top + TH)) shr 1);
  y := y - 10;
  if (Thumb.Image <> nil) and (Thumb.GotThumb) then
    Canvas.Draw(x, y, ThumbBmp(idx));
  R.Left := X;
  R.Top := Y;
  R.Right := X + TW;
  R.Bottom := Y + TH;
  Canvas.Pen.Color := CellBrdColor[F, S];
  InflateRect(R, 2, 2);
  Canvas.Rectangle(R);
  Canvas.Pen.Color := clWhite;
  InflateRect(R, -1, -1);
  Canvas.Rectangle(R);
  R := Cell;
  R.Top := R.Bottom - 20;
  txt := Thumb.Name;
  DrawText(Canvas.Handle, PChar(txt), Length(txt), R, DT_END_ELLIPSIS or
    DT_SINGLELINE or DT_NOPREFIX or DT_CENTER or DT_VCENTER);
end;

procedure TFrmPrincipal.ImgListViewMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
  Thumb: PThumbData;
begin
  vIdx := ImgListView.ViewIdx;
  i := ImgListView.ItemAtXY(Point(X, Y), False);
  if i <> -1 then
  begin
    Thumb := Items[ImgListView.Items[i]];
    lblArquivo.Caption := Thumb.Name;
  end
  else
    lblArquivo.Caption := '';
end;

procedure TFrmPrincipal.ImgListViewSelecting(Sender: TObject; Count: Integer);
begin
  UpdateStatus;
end;

function TFrmPrincipal.ObtemSerie(Diretorio: String): integer;
var
  Rec: TSearchRec;
  Res: Integer;
  nome:String;
  Serie : Integer;
begin
  Serie := 0;
  try
  Res := FindFirst(Diretorio + '\*.*', faAnyFile, Rec);
  while Res = 0 do
  begin
    Application.ProcessMessages;
    if ((Rec.Attr = faArchive) AND (VerificaTipo(Rec.Name))) then
    begin
      nome := Rec.Name;
      nome := copy(nome,0,(pos('.',nome)-1));
      nome := copy(nome,(pos(' ',nome)+1),(length(nome)-pos(' ',nome)));
      if Serie < strtoint(nome) then
        Serie := strtoint(nome);
    end;
    Res := FindNext(Rec);
  end;
  FindClose(Rec);
  except
    on E: Exception do
        //Erro
  end;
  Result := Serie;
end;

procedure TFrmPrincipal.OpenDir;
var
  Entry: PThumbData;
  SR: TSearchRec;
  n: Integer;
  Ext: string;
begin
  if Directory <> '' then
  begin
    Stop;
    ClearThumbs;
    ClearThumbsPool;
    ImgListView.ViewIdx := -1;
    ImgListView.Clear;
    Forms.Application.ProcessMessages;
    if Directory[length(Directory)] <> '\' then
      Directory := Directory + '\';
    if FindFirst(Directory + '*.*', faAnyFile - faDirectory, SR) = 0 then
    begin
      Items.Capacity := 1000;
      repeat
        Ext := LowerCase(ExtractFileExt(SR.Name));
        if (Ext = '.jpg') or (Ext = '.jpeg') then
        begin
          New(Entry);
          Entry.Name := SR.Name;
          Entry.Size := SR.Size;
          Entry.Modified := FileDateToDateTime(SR.Time);
          Entry.IWidth := 0;
          Entry.IHeight := 0;
          Entry.ThumbWidth := 0;
          Entry.ThumbHeight := 0;
          Entry.GotThumb := False;
          Entry.Image := nil;
          n := Items.Add(Entry);
          if n <> -1 then
            ImgListView.Items.Add(n);
        end;
      until FindNext(SR) <> 0;
      FindClose(SR);
      Items.Capacity := Items.Count;
    end;
  end;
  ImgListView.CalcView(True);
  SetThumbSize(SizerImg.Position, False);
  if ThumbThr = nil then
  begin
    ThreadDone := False;
    ThumbThr := ThumbThread.Create(ImgListView, Items);
  end;
  UpdateStatus;
end;

procedure TFrmPrincipal.pMVisualizaClick(Sender: TObject);
begin
  if (ImgListView.Items.Count = 0) then
    ShowMessage('� necess�rio selecionar uma pasta de origem com imagens primeiro!')
  else
    begin
      FrmVisualizador := TFrmVisualizador.Create(Self);
      try
        if FrmVisualizador.ShowModal = mrOk then
          ImgListView.UpdateView;
      finally
          FreeAndNil(FrmVisualizador);
      end;
    end;
end;

procedure TFrmPrincipal.btnBuscaDestinoClick(Sender: TObject);
var
  nSerie, oSerie:integer;
begin
  if BFFDestino.Execute then
    begin
      lblLocalDestDir.Caption := BFFDestino.Directory;
    end;
  oSerie := strtoint(edtSerie.Text);
  nSerie := ObtemSerie(BFFDestino.Directory);
  if (nSerie <> 0) AND (nSerie > oSerie) then
    if (MessageDlg('Foi detectada uma s�rie de n�mero maior na pasta, deseja atualizar para a atual?', mtConfirmation, mbOKCancel, 0) = mrOk) then
      begin
        edtSerie.Text := inttostr(nSerie);
      end;
end;

procedure TFrmPrincipal.btnBuscaOrigemClick(Sender: TObject);
begin
  if BFFOrigem.Execute then
    begin
      lblLocalOrig.Caption := BFFOrigem.Directory;
      Directory := BFFOrigem.Directory;
      OpenDir;
    end;
end;

procedure TFrmPrincipal.btnExecCopiaClick(Sender: TObject);
begin
  if (trim(lblLocalOrig.Caption)<>'') then
    begin
      if ImgListView.Selection.Count>=1 then
        begin
          if (trim(lblLocalDestDir.Caption)<>'') then
            begin
              CopiarArquivos(lblLocalOrig.Caption,lblLocalDestDir.Caption,edtNomePadrao.Text,strtoint(edtSerie.Text),pbCopia,BarraStatus);
            end
          else
            begin
              ShowMessage('Erro! � necess�rio selecionar o destino!');
            end;
        end
      else
        begin
          ShowMessage('Erro! � necess�rio selecionar uma imagem pelo menos!');
        end;
    end
  else
    begin
      ShowMessage('Erro! � necess�rio selecionar a origem!');
    end;
end;

procedure TFrmPrincipal.btnSelInvertClick(Sender: TObject);
begin
  ImgListView.SelectAll(True);
  UpdateStatus;
end;

procedure TFrmPrincipal.btnSelTudoClick(Sender: TObject);
begin
  ImgListView.SelectAll(false);
  UpdateStatus;
end;

procedure TFrmPrincipal.btnVisualizarClick(Sender: TObject);
begin
  pMVisualizaClick(Sender);
end;

procedure TFrmPrincipal.CarregaRegistro;
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\WestSoftwares\ImgCom',true);
  if padrao.ValueExists('NomePadrao') then
    rNomePadrao := padrao.ReadString('NomePadrao')
  else
    begin
      padrao.WriteString('NomePadrao','0');
      rNomePadrao := padrao.ReadString('NomePadrao');
    end;
  if padrao.ValueExists('NumSerie') then
    rSerie := inttostr(padrao.ReadInteger('NumSerie'))
  else
    begin
      padrao.WriteInteger('NumSerie',1);
      rSerie := inttostr(padrao.ReadInteger('NumSerie'));
    end;
  if padrao.ValueExists('UltimoDestino') then
    rUltimoDestino := padrao.ReadString('UltimoDestino')
  else
    begin
      padrao.WriteString('UltimoDestino','');
      rUltimoDestino := padrao.ReadString('UltimoDestino');
    end;
  BFFDestino.Directory := rUltimoDestino;
  hCodHD := padrao.ReadString('ProductCodAtiv');
  sNome := padrao.ReadString('ProductName');
  sSerial := padrao.ReadString('ProductSerial');
  padrao.CloseKey();
  padrao.Free;
end;

procedure TFrmPrincipal.ClearThumbs;
var
  i: Integer;
  Item: PThumbData;
begin
  ImgListView.Items.Clear;
  for i := Items.Count - 1 downto 0 do
  begin
    Item := Items[i];
    if Assigned(Item) then
      if Item.Size <> 0 then
        Item.Image.Free;
    Dispose(Item);
  end;
  Items.Clear;
end;

procedure TFrmPrincipal.ClearThumbsPool;
var
  i: Integer;
  Thumb: PCacheItem;
begin
  for i := ThumbsPool.Count - 1 downto 0 do
  begin
    Thumb := ThumbsPool[i];
    if Thumb.Bmp <> nil then
      Thumb.Bmp.Free;
    Dispose(Thumb);
  end;
  ThumbsPool.Clear;
  PoolSize := 0;
end;

procedure TFrmPrincipal.CMUpdateView(var message: TMessage);
begin
  ImgListView.Invalidate;
end;

function TFrmPrincipal.ConvertHextoDec(codHex: string): integer;
begin
  try
    Result := StrToInt('$' + codHex);
  except
    on Exception do
      Result := 0;
  end;
end;

function TFrmPrincipal.CopiarArquivos(Origem, Destino, NomePadrao: string;
  NumSerie: Integer; Gauge: TProgressBar; StatusBar : TStatusBar): Boolean;
var
  FromF, ToF: file of byte;
  Buffer: array [0 .. 4096] of char;
  NumRead: Integer;
  LengthArq: Longint;
  Rec: TSearchRec;
  Res: Integer;
  sFile: file of byte;
  ArqOri: String;
  ArqDst: String;
  Ext: String;
  iSerie, tArquivos: Integer;
begin
  Result := True;
  animaIcone.Active := True;
  LibBloq(False);
  iSerie := NumSerie + 1;
  Gauge.max:= 100;
  Gauge.Position := 100;
  Res := FindFirst(Origem + '\*.*', faAnyFile, Rec);
  tArquivos := 0;
  Gauge.Style := pbstNormal;
  Gauge.State := pbsPaused;
  while Res = 0 do
  begin
    Application.ProcessMessages;
    if (VerificaSelecionados(Rec.Name)) then
      begin
        ArqOri := Origem + '\' + Rec.Name;
        AssignFile(sFile, ArqOri);
        Reset(sFile);
        tArquivos := tArquivos + 1;
        CloseFile(sFile);
        BarraStatus.Panels[0].Text := 'Calculando...: ' + inttostr(tArquivos);
      end;
    Res := FindNext(Rec);
  end;
  BarraStatus.Panels[0].Text := 'Total : ' + inttostr(tArquivos);
  tArquivos := 0;
  BarraStatus.Panels[2].Text := 'Copiados : ' + inttostr(tArquivos);
  FindClose(Rec);
  Res := FindFirst(Origem + '\*.*', faAnyFile, Rec);
  Gauge.Style := pbstMarquee;
  Gauge.State := pbsNormal;
  while Res = 0 do
  begin
    Application.ProcessMessages;
    if (VerificaSelecionados(Rec.Name)) then
    begin
      ArqOri := Origem + '\' + Rec.Name;
      Ext := ExtractFileExt(Rec.Name);
      ArqDst := Destino + '\' + NomePadrao + ' ' + inttostr(iSerie) + Ext;
      AssignFile(FromF, ArqOri);
      Reset(FromF);
      LengthArq := FileSize(FromF);
      if VerificaArquivo(ArqDst) then
        begin
          Gauge.Style := pbstNormal;
          Gauge.State := pbsError;
          case ErroPersonalizado('O arquivo de nome ' + ExtractFileName(ArqDst) +
              ' j� existe, selecione uma das op��es a baixo:',mtInformation,
              mbYesNoCancel,['Substituir','At. S�rie','Parar C�pia'],
                'Arquivo j� existe!') of
            2:begin
                result := False;
                Gauge.State := pbsNormal;
                Gauge.Style := pbstNormal;
                break;
            end;
            6:begin
                AssignFile(ToF, ArqDst);
                rewrite(ToF);
                Gauge.Style := pbstMarquee;
                Gauge.State := pbsNormal;
                while LengthArq > 0 do
                begin
                  Application.ProcessMessages;
                  BlockRead(FromF, Buffer[0], SizeOf(Buffer), NumRead);
                  LengthArq := LengthArq - NumRead;
                  BlockWrite(ToF, Buffer[0], NumRead);
                end;
            end;
            7:begin
              iSerie := ObtemSerie(ExtractFilePath(ArqDst)) + 1;
              ArqDst := Destino + '\' + NomePadrao + ' ' + inttostr(iSerie) + Ext;
              AssignFile(ToF, ArqDst);
              rewrite(ToF);
              Gauge.Style := pbstMarquee;
              Gauge.State := pbsNormal;
              while LengthArq > 0 do
              begin
                Application.ProcessMessages;
                BlockRead(FromF, Buffer[0], SizeOf(Buffer), NumRead);
                LengthArq := LengthArq - NumRead;
                BlockWrite(ToF, Buffer[0], NumRead);
              end;
            end;
          end;
        end
      else
        begin
          AssignFile(ToF, ArqDst);
          rewrite(ToF);
          Gauge.Style := pbstMarquee;
          Gauge.State := pbsNormal;
          while LengthArq > 0 do
          begin
            Application.ProcessMessages;
            BlockRead(FromF, Buffer[0], SizeOf(Buffer), NumRead);
            LengthArq := LengthArq - NumRead;
            BlockWrite(ToF, Buffer[0], NumRead);
          end;
        end;
      inc(iSerie, 1);
      tArquivos := tArquivos + 1;
      BarraStatus.Panels[2].Text := 'C�piados : ' + inttostr(tArquivos);
      CloseFile(FromF);
      CloseFile(ToF);
      {if ckRecortar.Checked then
        DeleteFile(Origem + '\' + Rec.Name);}
    end;
    Res := FindNext(Rec);
  end;
  edtSerie.Text := inttostr(iSerie - 1);
  Gauge.State := pbsNormal;
  Gauge.Style := pbstNormal;
  FindClose(Rec);
  LibBloq(True);
  if Result then
    begin
      FrmTerminado := TFrmTerminado.Create(Self);
      try
        if (FrmTerminado.ShowModal = mrOk) then
          ImgListView.Selection.Clear;
      finally
        FreeAndNil(FrmTerminado);
      end;
    end
  else
      Showmessage('C�pia n�o completada com sucesso!' + #13 + 'Foram c�piados: '
       + IntToStr(tArquivos) + ' arquivos.');
  animaIcone.Active := False;
end;

procedure TFrmPrincipal.mExecutarCopiaClick(Sender: TObject);
begin
  btnExecCopiaClick(sender);
end;

procedure TFrmPrincipal.mFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  AtualizaRegistro;
end;

procedure TFrmPrincipal.FormCreate(Sender: TObject);
begin
try
  CarregaRegistro;
  if Autenticador.SerialIsCorrect(ConvertHextoDec(hCodHD),sNome,sSerial) then
    begin
      FrmPrincipal.Show;
    end
  else
    begin
      if not(Assigned(FrmVersaoteste)) then
        FrmVersaoteste := TFrmVersaoteste.Create(self);
      FrmVersaoteste.Show;
    end;
  Items := TList.Create;
  ThumbJPEG := TJpegImage.Create;
  ThumbJPEG.CompressionQuality := 80;
  ThumbJPEG.Performance := jpBestSpeed;
  ThumbSizeW := 255;
  ThumbSizeH := 255;
  ImgListView.CellWidth := ThumbSizeW + 20;
  ImgListView.CellHeight := ThumbSizeH + 40;
  CellJpeg := TJpegImage.Create;
  CellJPEG.Performance := jpBestSpeed;
  GenCellColors;
  CellStyle := -1;
  PoolSize := 0;
  MaxPool := Round(((Screen.Width * Screen.Height) * 3) * 1.5);
  Items := TList.Create;
  ThumbsPool := TList.Create;
  if ParamCount > 0 then
    begin
    if ParamCount = 2 then
      begin
        if ParamStr(2) = 'RepAuto' then
          begin
            BFFOrigem.Directory := ParamStr(1);
            if BFFOrigem.Execute then
              begin
                lblLocalOrig.Caption := BFFOrigem.Directory;
                Directory := BFFOrigem.Directory;
                OpenDir;
              end;
          end;
      end
    else
      begin
        lblLocalOrig.Caption := ParamStr(1);
        Directory := ParamStr(1);
        OpenDir;
      end;
    end;
  lblLocalDestDir.Caption := rUltimoDestino;

  edtNomePadrao.Text := rNomePadrao;
  edtSerie.Text := rSerie;
  lastSelection := -1;
except
  //esconder erro;
end;
end;

procedure TFrmPrincipal.FormDestroy(Sender: TObject);
var
  i: Integer;
  Item: PThumbData;
begin
  CellJpeg.Free;
  for i := Items.Count - 1 downto 0 do
  begin
    Item := Items[i];
    if Item.Size <> 0 then
      Item.Image.Free;
    Dispose(Item);
  end;
  ThumbsPool.Free;
  Items.Free;
end;

procedure TFrmPrincipal.FormResize(Sender: TObject);
begin
  AjustarTamanho;
end;

procedure TFrmPrincipal.FormShow(Sender: TObject);
begin
  AjustarTamanho;
end;

{ ThumbThread }

constructor ThumbThread.Create(xpThumbs: TrkView; Items: TList);
begin
  XPView := xpThumbs;
  XPList := Items;
  FreeOnTerminate := False;
  inherited Create(False);
  Priority := tpLower;
end;

procedure ThumbThread.Execute;
var
  Cnt, i: Integer;
  PThumb: PThumbData;
  Old: Integer;
  Update: Boolean;
  InView: Integer;
begin
  inherited;
  if (XPView.Items.Count = 0) then
    Exit;
  Cnt := 0;
  Old := XPView.ViewIdx;
  repeat
    while (Cnt < XPView.Items.Count) and (Terminated = False) do
    begin
      if XPView.ViewIdx <> Old then
      begin
        Cnt := XPView.ViewIdx - 1;
        if Cnt = -1 then
          Cnt := 0;
        Old := XPView.ViewIdx;
      end;
      PThumb := PThumbData(XPList.Items[Cnt]);
      Update := PThumb.GotThumb;
      if (not Update) and (not Terminated) then
      begin
        FrmPrincipal.ThumbsGetThumbnail(XPView, PThumb);
        InView := XPView.ViewIdx + (XPView.ViewColumns * (XPView.ViewRows));
        if (Cnt >= XPView.ViewIdx) and (Cnt <= InView) then
          PostMessage(FrmPrincipal.Handle, CM_UpdateView, 0, 0);
      end;
      inc(Cnt);
    end;
    Cnt := 0;
    for i := 0 to XPView.Items.Count - 1 do
      if PThumbData(XPList.Items[i]).GotThumb = False then
        inc(Cnt);
  until (Cnt = 0) or (Terminated);
  if not Terminated then
    PostMessage(FrmPrincipal.Handle, CM_UpdateView, 0, 0);
end;

end.
