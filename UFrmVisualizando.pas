unit UFrmVisualizando;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Buttons,
  rkView, Vcl.Imaging.pngimage, Vcl.ImgList, PngImageList;

type
  Arquivos = record
    diretorio : String;
    arquivo : String;
    tamanho : int64;
    Selecionado : boolean;
  end;
  TFrmVisualizador = class(TForm)
    pnImagem: TPanel;
    pnControles: TPanel;
    imgFoto: TImage;
    btnProximo: TButton;
    btnAnterior: TButton;
    imgSelOff: TImage;
    imgSelOn: TImage;
    GroupBox1: TGroupBox;
    lblNomeFoto: TLabel;
    lblTamanhoFoto: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnAnteriorClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure imgSelOffClick(Sender: TObject);
    procedure imgSelOnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnProximoKeyPress(Sender: TObject; var Key: Char);
  private
    sPasta:String;
    procedure updateCheck(hab : boolean);
  public
    listArquivos : TArray<Arquivos>;
    listSelecionados : Array of Integer;
    mark:integer;
  end;

var
  FrmVisualizador: TFrmVisualizador;

implementation

{$R *.dfm}

uses UFrmPrincipal;

function BytesToStr(const i64Size: Int64): string;
const
  i64GB = 1024 * 1024 * 1024;
  i64MB = 1024 * 1024;
  i64KB = 1024;
begin
  if i64Size div i64GB > 0 then
    Result := Format('%.1f GB', [i64Size / i64GB])
  else if i64Size div i64MB > 0 then
    Result := Format('%.2f MB', [i64Size / i64MB])
  else if i64Size div i64KB > 0 then
    Result := Format('%.0f kB', [i64Size / i64KB])
  else
    Result := IntToStr(i64Size) + ' byte';
end;

function VerificaTipo(nome: string): Boolean;
begin
  if (nome <> '.') and (nome <> '..') and (
      ((UpperCase(ExtractFileExt(nome)) = '.JPEG')) OR
      ((UpperCase(ExtractFileExt(nome)) = '.JPG')) ) then
  begin
    result := True;
  end
  else
    result := False;
end;

procedure TFrmVisualizador.btnAnteriorClick(Sender: TObject);
begin
  Dec(mark);
  imgFoto.Picture.LoadFromFile(listArquivos[mark].diretorio + '\' + listArquivos[mark].arquivo);
  FrmVisualizador.Caption := 'Visualizando - ' + listArquivos[mark].arquivo;
  FrmVisualizador.lblNomeFoto.Caption := listArquivos[mark].arquivo;
  FrmVisualizador.lblTamanhoFoto.Caption := BytesToStr(listArquivos[mark].tamanho);
  updateCheck(listArquivos[mark].Selecionado);
  if (mark = 0) then
    btnAnterior.Enabled := False;
  if (mark < Length(listArquivos)-1) then
    btnProximo.Enabled := True;

end;

procedure TFrmVisualizador.btnProximoClick(Sender: TObject);
begin
  Inc(mark);
  imgFoto.Picture.LoadFromFile(listArquivos[mark].diretorio + '\' + listArquivos[mark].arquivo);
  FrmVisualizador.Caption := 'Visualizando - ' + listArquivos[mark].arquivo;
  FrmVisualizador.lblNomeFoto.Caption := listArquivos[mark].arquivo;
  FrmVisualizador.lblTamanhoFoto.Caption := BytesToStr(listArquivos[mark].tamanho);
  updateCheck(listArquivos[mark].Selecionado);
  if (mark = Length(listArquivos)-1) then
    btnProximo.Enabled := false;
  if (mark > 0) then
    btnAnterior.Enabled := True;
end;

procedure TFrmVisualizador.btnProximoKeyPress(Sender: TObject; var Key: Char);
begin
  if ((Key = #83)) then
   begin
     if not listArquivos[mark].Selecionado then
        begin
          listArquivos[mark].Selecionado := True;
          imgSelOff.Visible := False;
          imgSelOn.Visible := true;
        end
      else
        begin
          listArquivos[mark].Selecionado := False;
          imgSelOff.Visible := true;
          imgSelOn.Visible := false;
        end;
   end;
   if ((Key = #33) AND (btnAnterior.Enabled)) then
    btnAnteriorClick(sender);
   if ((Key = #34) AND (btnProximo.Enabled)) then
    btnProximoClick(sender);
end;

procedure TFrmVisualizador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FrmPrincipal.atualizaSelecionados(listArquivos);
end;

procedure TFrmVisualizador.FormCreate(Sender: TObject);
var
  Rec: TSearchRec;
  Res: Integer;
  I: Integer;
begin
  SetLength(listArquivos,0);
  sPasta := FrmPrincipal.lblLocalOrig.Caption;
  if (FrmPrincipal.lastSelection = -1) then
    mark := 0
  else
    mark := FrmPrincipal.lastSelection;
  try
    Res := FindFirst(sPasta + '\*.*', faAnyFile, Rec);
    while Res = 0 do
    begin
      Application.ProcessMessages;
      if ((Rec.Attr = faArchive) AND (VerificaTipo(Rec.Name))) then
        begin
          SetLength(listArquivos,Length(ListArquivos)+1);
          listArquivos[Length(listArquivos)-1].arquivo := Rec.Name;
          listArquivos[Length(listArquivos)-1].diretorio := sPasta;
          listArquivos[Length(listArquivos)-1].tamanho := Rec.Size;
          listArquivos[Length(listArquivos)-1].Selecionado := false;
        end;
      Res := FindNext(Rec);
    end;
    FindClose(Rec);
    except
      on E: Exception do
          //Erro
    end;
    for I := 0 to FrmPrincipal.ImgListView.Selection.Count-1 do
      listArquivos[FrmPrincipal.ImgListView.Selection[i]].Selecionado := True;
end;


procedure TFrmVisualizador.FormShow(Sender: TObject);
begin
    FrmVisualizador.Caption := 'Visualizando - ' + listArquivos[mark].arquivo;
    FrmVisualizador.lblNomeFoto.Caption := listArquivos[mark].arquivo;
    FrmVisualizador.lblTamanhoFoto.Caption := BytesToStr(listArquivos[mark].tamanho);
    imgFoto.Picture.LoadFromFile(listArquivos[mark].diretorio + '\' + listArquivos[mark].arquivo);
    updateCheck(listArquivos[mark].Selecionado);
end;

procedure TFrmVisualizador.imgSelOffClick(Sender: TObject);
begin
  if not listArquivos[mark].Selecionado then
    begin
      listArquivos[mark].Selecionado := True;
      imgSelOff.Visible := False;
      imgSelOn.Visible := true;
    end
  else
    begin
      listArquivos[mark].Selecionado := False;
      imgSelOff.Visible := true;
      imgSelOn.Visible := false;
    end;
end;

procedure TFrmVisualizador.imgSelOnClick(Sender: TObject);
begin
  if listArquivos[mark].Selecionado then
    begin
      listArquivos[mark].Selecionado := False;
      imgSelOff.Visible := true;
      imgSelOn.Visible := false;
    end
  else
    begin
      listArquivos[mark].Selecionado := True;
      imgSelOff.Visible := False;
      imgSelOn.Visible := true;
    end;
end;

procedure TFrmVisualizador.updateCheck(hab: boolean);
begin
  imgSelOn.Visible := False;
  imgSelOff.Visible := False;
  if hab then
    imgSelOn.Visible := True
  else
    imgSelOff.Visible := True;
end;

end.
