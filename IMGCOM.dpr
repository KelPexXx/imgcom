program IMGCOM;

uses
  Forms,
  UFrmPrincipal in 'UFrmPrincipal.pas' {FrmPrincipal},
  UFrmSobre in 'UFrmSobre.pas' {FrmSobre},
  UFrmTerminado in 'UFrmTerminado.pas' {FrmTerminado},
  UFrmVersaoteste in 'UFrmVersaoteste.pas' {FrmVersaoteste},
  UFrmAtivacao in 'UFrmAtivacao.pas' {FrmAtivacao},
  UFrmForAtiv in 'UFrmForAtiv.pas' {FrmForAtiv},
  UFrmInfo in 'UFrmInfo.pas' {FrmInfo},
  Vcl.Themes,
  Vcl.Styles,
  UFrmVisualizando in 'UFrmVisualizando.pas' {FrmVisualizador};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Carbon');
  Application.Title := 'ImgCom v2.0';
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.ShowMainForm := False;
  Application.Run;
end.
