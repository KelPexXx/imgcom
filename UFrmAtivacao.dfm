object FrmAtivacao: TFrmAtivacao
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'ImgCom - Ativa'#231#227'o'
  ClientHeight = 169
  ClientWidth = 423
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object top: TPanel
    Left = 0
    Top = 0
    Width = 423
    Height = 118
    Align = alClient
    TabOrder = 0
    object lblAtivacao: TLabel
      Left = 107
      Top = 16
      Width = 213
      Height = 21
      Caption = 'Insira o n'#250'mero de ativa'#231#227'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object edtSerial: TLabeledEdit
      Left = 80
      Top = 83
      Width = 313
      Height = 21
      EditLabel.Width = 57
      EditLabel.Height = 21
      EditLabel.Caption = 'Serial : '
      EditLabel.Font.Charset = DEFAULT_CHARSET
      EditLabel.Font.Color = clWindowText
      EditLabel.Font.Height = -17
      EditLabel.Font.Name = 'Tahoma'
      EditLabel.Font.Style = []
      EditLabel.ParentFont = False
      LabelPosition = lpLeft
      TabOrder = 0
    end
  end
  object botton: TPanel
    Left = 0
    Top = 118
    Width = 423
    Height = 51
    Align = alBottom
    TabOrder = 1
    object btnAtivar: TJvImgBtn
      AlignWithMargins = True
      Left = 332
      Top = 4
      Width = 87
      Height = 43
      Align = alRight
      Caption = 'Ativar'
      ImageIndex = 0
      Images = imagens
      TabOrder = 0
      OnClick = btnAtivarClick
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
    end
    object btnFechar: TJvImgBtn
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 75
      Height = 43
      Align = alLeft
      Caption = 'Fechar'
      ImageIndex = 1
      Images = imagens
      TabOrder = 1
      OnClick = btnFecharClick
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
    end
    object btnVoltar: TJvImgBtn
      AlignWithMargins = True
      Left = 239
      Top = 4
      Width = 87
      Height = 43
      Align = alRight
      Caption = 'Voltar'
      ImageIndex = 2
      Images = imagens
      TabOrder = 2
      OnClick = btnVoltarClick
      HotTrackFont.Charset = DEFAULT_CHARSET
      HotTrackFont.Color = clWindowText
      HotTrackFont.Height = -11
      HotTrackFont.Name = 'Tahoma'
      HotTrackFont.Style = []
    end
  end
  object edtNome: TLabeledEdit
    Left = 80
    Top = 51
    Width = 241
    Height = 21
    EditLabel.Width = 57
    EditLabel.Height = 19
    EditLabel.Caption = 'Nome : '
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clWindowText
    EditLabel.Font.Height = -16
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = []
    EditLabel.ParentFont = False
    LabelPosition = lpLeft
    TabOrder = 2
  end
  object imagens: TPngImageList
    Height = 32
    Width = 32
    PngImages = <
      item
        Background = clWindow
        Name = '1392608889_Key'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
          F40000068B4944415478DABD577B50545518FF9D5D169087288FB55C144DD950
          4BB29CB007353929D963AD2C4B339B5E333593934D338E396599A68EA3654DE1
          131F84A694822005220129B65A0ACA42200AA9B004CBBAABBBECC672F7DEBE73
          F7B262F5070BD69DFDE69CFB9D7BCEEFF73DCE77CE320CF0693980F0C1E13088
          12A669D4981CA4C668AEFFD3838B0E174EB7DB70247D3F72B7E4A183D40289D4
          7B3E1B08B8B384A5D17A6F86EAE6CE64D169606113004D1CA9BC903C66489D67
          20D98AF1FB999C0A5313B63FF51EF6D134078977C004AE14639E266AD227C109
          0B47AAA2A793464DC0A26F6DC92BB7127F17FF241285B0D66FB6961DADDFF0DC
          52A4D3A045F146FF08D88B589A26EA8ECD21FAE5235591297F03167D1EE84584
          B7E2950A584C9FDAB3721AD62D4A47060DB4F3C18009347D8BF0A808B62B7252
          C64C75CC63D7AC755F84D7B21FA2B5145E9709AAB0F15045A7421DFB0858F030
          F93B6F47018E7FBFBCEEA515D23B8D6618B9230326D0968F398347CDDE1D92B8
          065005F9AC755F80BD6EADB7CA98673F7A06AD7B4AD0903802EA27EEC3488361
          C6B898A4D74210AC259E2E78CEAFC2DAF4F2CCA519D26A5AEE5CC004CCB9D816
          7BF7C697836266F85D2F7A9DD438215EAD86603B8A9A53A58D5BF699B337E7A1
          EABB1578F9D127E7A6696E7E94BE7743683B8CFCFD7B6B677F804534B9226002
          7F1CC099D8077FBC5D153A524934C11706DEF277E132044B31CE551D6CDA955B
          FF457828DC0BE6C56F1CA47F058C31E2D90C53D956FBE4D7B1982614044CA03D
          0F8ED887AB22C0343238E32110DA4828A7840EF2868D325B82C77E16A6E3C50D
          CB36D93FDEF9BEFAEB219316CBF325B10B6DC7D709239E913EA2D7ECC04390C3
          1CC3A61923806EDAEB97A8699509F8EB4B4F99216B3B2F9520F39B93F9730DC3
          9F884A9A270F4ADE2E988D9F0BA366632D29B20226707E37ABD1DDBF6CBC5AC3
          3D6EF36D643F28AE23E1B1FF8E8AC2DCE694D47BE3C37429F240B7B315278BF7
          3A5217485FF58B40E556B65B3FE5F139C15109D7944CA21F53B0A9AFB0F276BB
          6169A81062C7A404A943227D5E69ADC29E3D1517DF58276DEE57080AD7B0371F
          34CC4AD78447CB86FA1D20297D76CD11A25790496842237DE4BCDDE8A82BC692
          F5CDC6ED85D881FE24E1DE0F596A9A616A4944DC088D9F819F46EF78C0EF8B1E
          CF389A6B7030FFD7F6B73E97F2AEBA908B40B761CD3696A40E62E5BAE4A9DA41
          437975F333E815FCEB49283182B3AD09BF5555BB56EE709CCC3F861CEE4C0452
          884E6F6149411CFCB6146DE42DAFA2A56A1B74C9CF43721EF1D94AF195A46B0E
          E9E1260902ECE646D4559F776DC975D6661EC2211A39CCD3097D2DC5A736FAC0
          E327DEA58D1AF31A81674037710ED065A2BD6F91C1BC4237BADD9D08098B90D1
          BB5C4EB8AED8D1D9D186C3472DED078E49F5078DE06C8FF125D1D7C3E897AF58
          9286C0139293B583C72AE0C904EEE6E0EDE8B45BD17AAE49FCE9E78E8E9BA211
          123704E17C5E9B8DB9CFB648B6CA06C99C6F84892E27B5A4AE22A9435F8F63E3
          172AB21CE5A3EF98A01DA27F1D2D955BE9641B8FF69A6CB9DAF1D9E7CDECEA7B
          19A2F19C19F58A557CCD3065093A24E49B50334923492BFA7A21A9584FE06A56
          3EF64EBD7668E20B6839BD1BC36F33A0B6E84B3CF096B3C8E6448BB2583709EF
          9B14201EFD2065190F895B21D289BE5EC98EACA36C2770FD5D63B5D1FA79049E
          05DDED06C07504D565263CB4B033EBB20305F4E9598500276255802404F0FC83
          40E91A265B9E3439411B93F4229A093C7E5C2A255CB5BC7665691DA6BFEBDE64
          BD2ADF6A4EF576677F9EEB0894ACE66E47F9B8BBE3B5318906984DF9D0717077
          255538864B7516FC50D46E5DB041DA2178B18BA69C867C07BB01048A5712B80A
          E5E3536ED2C6E967A2C594079D5E4F0E36CBE3177EB3A2A0E8B275518658E2EA
          4229A97E20B918A8CBFF9540D1729FE513A6C469B5B7F6802782795AE42F1A6B
          ED2828B459176F973838BFCB9591F0AC770D045C2670F0439610AC612726DE13
          A31D96644073753E1C56B7EF5021DB3C1E11653FBBAD4B768A7F07770FD47A99
          40CE12D5CCC808963B75FE7C8AF9010C1F1582BCAC36E1C9152277F11FF06D9F
          2E9226921337125C2690F5AE6A96369A7DF7F0AC187F42E4645A84A7574A5BA9
          5B0C5F01E1FBF70A7C5BED8681CB04B6BFAD7A76789C2A7BFAAC28FF59B62FD3
          263CB34AFC8CBA5F9354DF28B07F2590B140F5ECB0A1C81E3126D86F577D9D47
          98BDFA7F22005FDD4E26A10D8F9B153DDFDBBCCAF584E03F25C0EB761409FDAD
          45A8A2E7BEE0E5955F773BFF4B027F0189A70B52964DE7DA0000000049454E44
          AE426082}
      end
      item
        Background = clWindow
        Name = '1385689820_system-log-out'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
          F4000006DF4944415478DA9D97794C147714C7BFB3C2822C8B800B22B7BA29A2
          49558CD1C43335418316ABF528AE060D2858AB36DA2A22696A8FC4AA35AD4D6B
          CBA1E091B4F11F6D9A6AA214634C6C3546E28577318A226095C8B2B30BBBD3F7
          7E3B436761D6A3BFCDCBCCFE76F6F73EEFF8BDDF1B495114FC8F2191F4233193
          84A8F7DAE005BD245DAAF8D439E38518C0B134EFCBAEAEAED257D22C4930994C
          080B0B43FFFE11080F0F87393454CC29F4F17A7DF0783CFCFBB7DBB7EDE035DD
          2A50708085EFCD57F2972EC78D1B37E072B9403070BBDD6221BED7A4BBBB9B14
          78111212028B2502EBD6AD4362E26001E5770A994AEB75767662C7D7DBB17DDB
          CE4134D54EE209E6851E8082E52B70FDFA75C8B21CA05CBBB272169FCF07B3D9
          8C989818AC5EBD0A365B9C00F043F801F8996F767F83CD9B4A87D1543389AC86
          2238C0AAA2D5B876ED9A50CE107AE57CD500F879767F7C7C3C0A0B0B101B1B2B
          DCFF9F077C02A0BCB21C6B56AF1D4953F7499C2F05F870ED7A5CBD7A55848021
          B430E8DDAF0718346810962DCB179E0804504498AA6BF6A268E5AA3769EA1E49
          C74B01367E541200C056EB3DC08BF2BD1E60E9D2251830608000E010F086F287
          C08B7D35FB50B4A27834E9F8FB9500367DBC190D0D0D3D8A390CFAF86B6160F7
          6A21703816232A2A8A00FA8153400F505D538D952B8A18A0F1B71123BCED0D0D
          190E45B91014A06463A900D0BBBCB717F44918171787BCBC3C58AD91860035FB
          ABB1A2B068F4B59D3B5B2F6DDCF8AB4F5132172B8A2528C0E64D5BC42ED05BAD
          BF6AA201D86C362C5AB4109191C101DE193E7252EDB4697BECF3E665DE3F73C6
          3BF7E1C3F0A000A525653D0046CAB57B7E3E940A0F032C58309F00AC6A0E0402
          ECFFF107446D2EBB63CFCD4D1BB27871C8A9E262F7DCA6A6170370213202E02B
          27594A4A0A9C4E279E3E7D2A2C9F3FFF5D4300B9AD1547C76461C4D4A9DE94EC
          EC7E1255CBD31B3674C9CDCDAE5EFA9B02006EDEBCD9C762ED9E5D6FB7DB85F5
          7CDFD1D181993367AA21F003F070B5B4E28FC9939134660C92264E849B7656C4
          D8B1A0BA0DF076F55342A1354F14147803006EDDBAD507407F6D6B6B13593F7E
          FC7801316EDCB80000B9A50527274E42F2A851183C7A346452AED0F6256A48FD
          FCE79542FFA3C5609D3E1D278A8B03016EDFBE1D34F6DA95639F9999897BF7EE
          212727A707406E7E84DA496439FD164FC2CAC1CAE9DC10CA45B1E26AE0135051
          D9D9A85DBFBE2F8056F18C94F3C9C7D6DFBD7B172D64ED92250E3A9422E1696D
          C5C9091390346C186C438640A692CE8A24B21CA49C01144E609A33510D110033
          66A0AEACAC2F00BBD6C87A7E2E2B2B4B24E1D9B36791949424760103B45FBE8C
          BA2953309CE26EA65878EEDF8744962B54517D74327A295FF85E643DCD9B2222
          10EB70E0CC9E3D7D01F8BB91F51919192201EBEAEA44254C4E4E466EEEDB0280
          43D074F408CEE52F837DE85098690D1787C067587DC58859B0007F1E3E6C0CD0
          3B0C5C7858617D7DBD382B2C168B28C5B367CF123920492671DC371E398A0BF9
          F9B0D3760D2748F9CA157F126AF1D78DE8DC5CFC75E48831801686DEC9C88714
          D703AD12CE9A9523601840965D0427A3E9D8315C2F2AC2B0840458E8A0624F58
          68B748F41F069118469573C78F1B03040B83FE2C6080E9D3DF421429A176441C
          5E2E57279C14F3E6DA5A347EB006763A2FA24814F2C6A58B17BBBC1E4F682F47
          3C3104E061B415F948D68EE38103078AB0A4A7A78BA6C4E371532BE622718A22
          F5FBAE5D483D70501E6AB38547A6A5E1527DBDD3D1D53555ED8E5C9A0405D0CE
          7FBD729F9A54BC1DD9F5B6381B5208827704276227E5878B3CE07476E297C33F
          23BAF65489EDF4E94F52A2A3231E3C7FEE74747767C1DF1F7A5490E741015899
          968CDA36D43A1EFE1E1B1B83D4D45421FE9EC0043A72E196DDE87076E0E0A103
          48881F3CA575EBD684B4070FAA7949076087BF55EF2471D35A4A5000ADB562D1
          9ACE67CF9E525BA3203D2D9D5C9F265CCFE130E9B25C03AFAAAA4471D1FB6F9C
          3F7FBEA569CE9CCC30B73B7B09F0B9D2EB45C41040EB7079700BDED2DA029912
          8CE3CD165BA3AC0833875191A32A67D29E95C456F4F9089C002AAA2AF0D9A75F
          A4D2641BFCEF062F6EC94A4BB610C01D31C91659AD562AB78FF1E49F36616D62
          522222A88285869AFD8AF923F55DD0A77AAEBCBC023BBE7A8DF702EE881A1B1B
          4593D9D1F11C8FE870494A4E10D91E1E168E107EFB914CAAC5127A744B814B8B
          D0514352FE5305BEDBFD7D0CFC0D6977B08A1800D0DEFE0C8F1E3F147D1E5738
          73185B4BB59B4D9554A5AAD97AE3034C530B197BA0B27CAF05AFFA6A56585048
          EEEB16EEE5B86BADB6960F9291BF0D06A71803545656E140CD21B36AFDABBD9C
          6A0AF5F232E5FEF701A51784E81BB755EFDDBF2558F20500F49E0B727D9DA1E8
          E485E35F8DFF8A58AAC39C720000000049454E44AE426082}
      end
      item
        Background = clWindow
        Name = '1392396040_Arrow Left'
        PngImage.Data = {
          89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
          F400000A41694343504943432050726F66696C65000078019D96775453D91687
          CFBD37BDD012222025F41A7A0920D23B48150451894980500286842676440546
          141129566454C0014787226345140B838262D709F21050C6C1514445E5DD8C6B
          09EFAD35F3DE9AFDC759DFD9E7B7D7D967EF7DD7BA0050FC8204C27458018034
          A15814EEEBC15C1213CBC4F7021810010E5801C0E166660447F84402D4FCBD3D
          9999A848C6B3F6EE2E8064BBDB2CBF502673D6FF7F912237432406000A45D536
          3C7E2617E5029453B3C51932FF04CAF495293286313216A109A2AC22E3C4AF6C
          F6A7E62BBBC9989726E4A11A59CE19BC349E8CBB50DE9A25E1A38C04A15C9825
          E067A37C0765BD54499A00E5F728D3D3F89C4C003014995FCCE726A16C893245
          1419EE89F202000894C439BC720E8BF939689E0078A667E48A04894962A611D7
          9869E5E8C866FAF1B353F962312B94C34DE188784CCFF4B40C8E301780AF6F96
          450125596D996891EDAD1CEDED59D6E668F9BFD9DF1E7E53FD3DC87AFB55F126
          ECCF9E418C9E59DF6CECAC2FBD1600F6245A9B1DB3BE955500B46D0640E5E1AC
          4FEF2000F20500B4DE9CF31E866C5E92C4E20C270B8BECEC6C73019F6B2E2BE8
          37FB9F826FCABF8639F799CBEEFB563BA6173F81234915336545E5A6A7A64B44
          CCCC0C0E97CF64FDF710FFE3C03969CDC9C32C9C9FC017F185E85551E8940984
          8968BB853C8158902E640A847FD5E17F18362707197E9D6B1468755F007D8539
          50B84907C86F3D00432303246E3F7A027DEB5B10310AC8BEBC68AD91AF738F32
          7AFEE7FA1F0B5C8A6EE14C412253E6F60C8F647225A22C19A3DF846CC1021290
          0774A00A34812E30022C600D1C80337003DE2000848048100396032E48026940
          04B2413ED8000A4131D80176836A7000D4817AD0044E823670065C0457C00D70
          0B0C8047400A86C14B3001DE81690882F01015A241AA9016A40F9942D6101B5A
          0879434150381403C5438990109240F9D026A8182A83AAA143503DF423741ABA
          085D83FAA007D0203406FD017D84119802D3610DD800B680D9B03B1C0847C2CB
          E04478159C0717C0DBE14AB8163E0EB7C217E11BF0002C855FC2930840C80803
          D14658081BF144429058240111216B9122A402A9459A900EA41BB98D489171E4
          030687A161981816C619E387598CE1625661D6624A30D5986398564C17E63666
          103381F982A562D5B1A65827AC3F760936119B8D2DC456608F605BB097B103D8
          61EC3B1C0EC7C019E21C707EB8185C326E35AE04B70FD78CBB80EBC30DE126F1
          78BC2ADE14EF820FC173F0627C21BE0A7F1C7F1EDF8F1FC6BF2790095A046B82
          0F219620246C2454101A08E708FD8411C2345181A84F7422861079C45C6229B1
          8ED841BC491C264E93144986241752242999B48154496A225D263D26BD2193C9
          3A6447721859405E4FAE249F205F250F923F50942826144F4A1C4542D94E394A
          B94079407943A5520DA86ED458AA98BA9D5A4FBD447D4A7D2F47933397F397E3
          C9AD93AB916B95EB977B254F94D79777975F2E9F275F217F4AFEA6FCB80251C1
          40C15381A3B056A146E1B4C23D8549459AA2956288629A62896283E235C55125
          BC928192B7124FA940E9B0D225A5211A42D3A579D2B8B44DB43ADA65DA301D47
          37A4FBD393E9C5F41FE8BDF4096525655BE528E51CE51AE5B3CA5206C23060F8
          335219A58C938CBB8C8FF334E6B9CFE3CFDB36AF695EFFBC2995F92A6E2A7C95
          2295669501958FAA4C556FD514D59DAA6DAA4FD4306A266A616AD96AFBD52EAB
          8DCFA7CF779ECF9D5F34FFE4FC87EAB0BA897AB8FA6AF5C3EA3DEA931A9A1ABE
          1A191A551A9734C635199A6E9AC99AE59AE734C7B4685A0BB5045AE55AE7B55E
          309599EECC546625B38B39A1ADAEEDA72DD13EA4DDAB3DAD63A8B35867A34EB3
          CE135D922E5B3741B75CB75377424F4B2F582F5FAF51EFA13E519FAD9FA4BF47
          BF5B7FCAC0D020DA608B419BC1A8A18AA1BF619E61A3E16323AA91ABD12AA35A
          A33BC63863B6718AF13EE35B26B0899D4992498DC94D53D8D4DE5460BACFB4CF
          0C6BE6682634AB35BBC7A2B0DC5959AC46D6A039C33CC87CA3799BF92B0B3D8B
          588B9D16DD165F2CED2C532DEB2C1F59295905586DB4EAB0FAC3DAC49A6B5D63
          7DC7866AE363B3CEA6DDE6B5ADA92DDF76BFED7D3B9A5DB0DD16BB4EBBCFF60E
          F622FB26FB31073D877887BD0EF7D8747628BB847DD511EBE8E1B8CEF18CE307
          277B27B1D349A7DF9D59CE29CE0DCEA30B0C17F017D42D1872D171E1B81C7291
          2E642E8C5F7870A1D455DB95E35AEBFACC4DD78DE776C46DC4DDD83DD9FDB8FB
          2B0F4B0F91478BC794A793E71ACF0B5E8897AF579157AFB792F762EF6AEFA73E
          3A3E893E8D3E13BE76BEAB7D2FF861FD02FD76FADDF3D7F0E7FAD7FB4F043804
          AC09E80AA404460456073E0B320912057504C3C101C1BB821F2FD25F245CD416
          0242FC4376853C09350C5D15FA73182E2C34AC26EC79B855787E7877042D6245
          4443C4BB488FC8D2C8478B8D164B167746C947C545D5474D457B4597454B9758
          2C59B3E4468C5A8C20A63D161F1B157B247672A9F7D2DD4B87E3ECE20AE3EE2E
          335C96B3ECDA72B5E5A9CBCFAE905FC159712A1E1B1F1DDF10FF8913C2A9E54C
          AEF45FB977E504D793BB87FB92E7C62BE78DF15DF865FC91049784B284D14497
          C45D896349AE491549E3024F41B5E075B25FF281E4A9949094A32933A9D1A9CD
          6984B4F8B4D34225618AB02B5D333D27BD2FC334A33043BACA69D5EE5513A240
          D1914C28735966BB988EFE4CF5488C249B2583590BB36AB2DE6747659FCA51CC
          11E6F4E49AE46ECB1DC9F3C9FB7E356635777567BE76FE86FCC135EE6B0EAD85
          D6AE5CDBB94E775DC1BAE1F5BEEB8F6D206D48D9F0CB46CB8D651BDF6E8ADED4
          51A051B0BE6068B3EFE6C642B94251E1BD2DCE5B0E6CC56C156CEDDD66B3AD6A
          DB97225ED1F562CBE28AE24F25DC92EBDF597D57F9DDCCF684EDBDA5F6A5FB77
          E0760877DCDDE9BAF3589962595ED9D0AEE05DADE5CCF2A2F2B7BB57ECBE5661
          5B71600F698F648FB432A8B2BD4AAF6A47D5A7EAA4EA811A8F9AE6BDEA7BB7ED
          9DDAC7DBD7BFDF6D7FD3018D03C5073E1E141CBC7FC8F7506BAD416DC561DCE1
          ACC3CFEBA2EABABF677F5F7F44ED48F191CF478547A5C7C28F75D53BD4D737A8
          379436C28D92C6B1E371C76FFDE0F5437B13ABE95033A3B9F804382139F1E2C7
          F81FEF9E0C3CD9798A7DAAE927FD9FF6B6D05A8A5AA1D6DCD689B6A436697B4C
          7BDFE980D39D1DCE1D2D3F9BFF7CF48CF6999AB3CA674BCF91CE159C9B399F77
          7EF242C685F18B8917873A57743EBAB4E4D29DAEB0AEDECB8197AF5EF1B972A9
          DBBDFBFC5597AB67AE395D3B7D9D7DBDED86FD8DD61EBB9E965FEC7E69E9B5EF
          6DBDE970B3FD96E3AD8EBE057DE7FA5DFB2FDEF6BA7DE58EFF9D1B038B06FAEE
          2EBE7BFF5EDC3DE97DDEFDD107A90F5E3FCC7A38FD68FD63ECE3A2270A4F2A9E
          AA3FADFDD5F8D766A9BDF4ECA0D760CFB388678F86B8432FFF95F9AF4FC305CF
          A9CF2B46B446EA47AD47CF8CF98CDD7AB1F4C5F0CB8C97D3E385BF29FEB6F795
          D1AB9F7E77FBBD6762C9C4F06BD1EB993F4ADEA8BE39FAD6F66DE764E8E4D377
          69EFA6A78ADEABBE3FF681FDA1FB63F4C791E9EC4FF84F959F8D3F777C09FCF2
          78266D66E6DFF784F3FBA5F22622000004304944415478DAC5976D685B6514C7
          CF73F3D2242B765DB3B5D9BA55F00505C5A9F8B2F9C2151115751F667128FBD0
          CAD4551B990A5BB814A4DAD6D9DADAAE85BA364BC3F48B826EFA65033F0C6528
          149482E08782D9BC2B4D1A9B2EDB9ADCE6E5E67A4E9E7BD7244B5FA4F6EEC293
          FB70F3DC737EE77FCE799E84699A0637F3626B0560CF349D03A7DDA1FD30B2CB
          740072EE1D0C8A677B7CA1C913476F3315803DDBF4937720F864F30E80BDADBE
          D08580890086F3C67A80840AF0D67BBE906C16007BAEF9676FFFD8132FD60144
          3300160670E4B04900EC8503E7BDBDFEC79EDE0C3095025072005556808F245F
          E8D27A03B097DEF8C5DB33BA6BB71BE05212601EA5C7E0615305C0D1365F687A
          3D01D89E377FF5768F3CFA7035C0DF0AC0B534400E5F130404B003F4B6FB4233
          EB0560DBDB32DED235FCD0CE8D3CF2ABE83CA3BF42F9AF4280604FFB5C3A393F
          8B3C20ACD2716C5E994F7C3BF4E0B200AE975B7F3BD039F4C05DB7004CEBCEB3
          287D4EFF9E9C396D009B1D006E1C2EAC072B81E5432BB625E8C00E9C387141D3
          0792FCD74857C3920095FBDE9D78BDFDD87DB75772E724BB9AE3C30020831634
          6847830E2B9F5B6EF4CDA5C68756FCB2C2C2D3D6D126C917479700D8F4EAA13F
          F67FD87F4F838B3B4F1A916B7C688613FC10189FD310F4C9B20008EA76020C7D
          2CC953E500DCFBDFFFF3B5B6BEBB3DB82892C056A39C53D439EEB89497157863
          374C8AD7D9502107A6CC8D811DEF94E4697F09C0D6E6C3938D47BAEFA8D59D27
          B1D73359945D5FB296732B0F60E100351B00029F4872B810C0DEE8FDFD60C7E0
          FDDBF59C27B0DD16328B7987B59EDA0600E6BF0615087E2AC99142003AD95A7A
          86C58A4C1CAE2EA0F4187D3ACB73AFE6FE3F05ECA840FDAD1EF8AA14E03AC467
          C36236115731F796549A43A87A01728AF2D1AD06C02A2C027CD92DC9FFF8CB14
          2141BCDD7B5C4C5E9BCD2CA4041B41501DE48C42D4169520A34CAF7C8115F395
          6E488C5A9414C02ED88600275181987F8936248877FA46C52B576613988A0D19
          1D42558BEB21DF8202DF92E96EB4A201552A1201D85081AD0D1E087649F265FF
          321B511EA2DF2FC663D1B892661BD3B417643948A10224AB60E53D2E18306537
          02BE8646ED760F8C75AC00B00871429CBB3C135514B685D2A1663988212B495A
          595B37E172B2712A328B554F493983FA4317DEFB7C8776E7BEEEBF77C5C38820
          5A070262742E222B49B68394C864B80A146DF5360F9C1A9464353C1D5AB9148B
          2FEDC7E053AB3B8E09E258400CC7662617927027415041920235F51EF87E4092
          95B1AE86FF0A90B7BDEA1F24798831313C179948266027A581724D00A73F97E4
          64609D010C082F424CC5C2E75369F6B8862A5463417D872DA59801500871213E
          73269BD59EAFAAC31AC07D3D6516C07508FC43723116F9C6B5A5EE15D3010A21
          A236183AD529ED311DA01062D077705C3BFDC523A603181074A79EBE29006BBD
          FE0521AE32DFC87041570000000049454E44AE426082}
      end>
    Left = 368
    Top = 8
    Bitmap = {}
  end
  object Autenticador: TJvSerialMaker
    Base = 0
    Left = 352
    Top = 24
  end
end
