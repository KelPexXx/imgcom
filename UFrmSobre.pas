unit UFrmSobre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, pngimage, ExtCtrls, JvComponentBase, JvAnimTitle,
  JvExStdCtrls, JvBehaviorLabel, JvWinampLabel, JvExControls, JvWaitingGradient,
  JvGradient, JvStarfield, JvAnimatedImage, JvGIFCtrl, JvSwitch, Vcl.ImgList,
  PngImageList, JvBmpAnimator;

type
  TFrmSobre = class(TForm)
    logo: TImage;
    lblVersao: TLabel;
    lblEmp: TLabel;
    fundo: TJvGradient;
    lblRelease: TLabel;
    JvBmpAnimator1: TJvBmpAnimator;
    imgLogoImgCOm: TPngImageList;
    Image1: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure JvBmpAnimator1Click(Sender: TObject);
    procedure Image1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmSobre: TFrmSobre;

implementation

{$R *.dfm}


procedure TFrmSobre.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmSobre);
end;

procedure TFrmSobre.Image1Click(Sender: TObject);
begin
  image1.Visible := not image1.Visible;
  fundo.Visible := not fundo.Visible;
  logo.Visible := not logo.Visible;
  JvBmpAnimator1.Active :=  not JvBmpAnimator1.Active;
  JvBmpAnimator1.Visible := not JvBmpAnimator1.Visible;
  lblVersao.Visible := not lblVersao.Visible;
  lblEmp.Visible := not lblEmp.Visible;
end;

procedure TFrmSobre.JvBmpAnimator1Click(Sender: TObject);
begin
  fundo.Visible := not fundo.Visible;
  logo.Visible := not logo.Visible;
  JvBmpAnimator1.Active :=  not JvBmpAnimator1.Active;
  JvBmpAnimator1.Visible := not JvBmpAnimator1.Visible;
  lblVersao.Visible := not lblVersao.Visible;
  lblEmp.Visible := not lblEmp.Visible;
  image1.Visible := not image1.Visible;
end;

end.
