unit UFrmAtivacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, JvExMask, JvToolEdit, JvMaskEdit, ExtCtrls, ImgList,
  PngImageList, JvExStdCtrls, JvButton, JvCtrls, JvComponentBase, JvSerialMaker,
  Registry;

type
  TFrmAtivacao = class(TForm)
    top: TPanel;
    botton: TPanel;
    lblAtivacao: TLabel;
    edtSerial: TLabeledEdit;
    imagens: TPngImageList;
    btnAtivar: TJvImgBtn;
    btnFechar: TJvImgBtn;
    btnVoltar: TJvImgBtn;
    edtNome: TLabeledEdit;
    Autenticador: TJvSerialMaker;
    procedure btnFecharClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    function GetSerieHDdec(SerialHD:string):integer;
    procedure btnAtivarClick(Sender: TObject);
    procedure AtivaCorreto;
    procedure FormCreate(Sender: TObject);
  private
      sCodAtiva,sSerialAtiv,sNome:string;
  public
    { Public declarations }
  end;

var
  FrmAtivacao: TFrmAtivacao;

implementation

uses UFrmForAtiv;

{$R *.dfm}

{ TFrmAtivacao }

procedure TFrmAtivacao.AtivaCorreto;
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\WestSoftwares\ImgCom',true);
  padrao.WriteString('ProductCodAtiv',sCodAtiva);
  padrao.WriteString('ProductName',sNome);
  padrao.WriteString('ProductSerial',sSerialAtiv);
  padrao.CloseKey();
  padrao.Free;
end;

procedure TFrmAtivacao.btnAtivarClick(Sender: TObject);
begin
  if Autenticador.SerialIsCorrect(GetSerieHDdec(sCodAtiva)
    ,edtNome.Text,edtSerial.Text) then
      Begin
        ShowMessage('Ativa��o completa!');
        sSerialAtiv := edtSerial.Text;
        sNome := edtNome.Text;
        AtivaCorreto;
        Application.Terminate;
      End
  else
    ShowMessage('Serial ou nome incorretos!');
    edtNome.SetFocus;
end;

procedure TFrmAtivacao.btnFecharClick(Sender: TObject);
begin
  Close;
  Application.Terminate;
end;

procedure TFrmAtivacao.btnVoltarClick(Sender: TObject);
begin
  FrmForAtiv.Show;
  FreeAndNil(FrmAtivacao);
end;

procedure TFrmAtivacao.FormCreate(Sender: TObject);
begin
  sCodAtiva := FrmForAtiv.GetSerialVolNum('C');
  edtNome.Text := FrmForAtiv.edtNomeAtiv.Text;
end;

function TFrmAtivacao.GetSerieHDdec(SerialHD: string): integer;
begin
  try
    Result := StrToInt('$' + SerialHD);
  except
    on Exception do
      Result := 0;
  end;
end;

end.
