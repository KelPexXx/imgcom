unit UFrmVersaoteste;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, pngimage, JvExStdCtrls, JvButton, JvCtrls,
  JvExControls, JvgProgress, ImgList, PngImageList, Registry, JvComponentBase,
  JvSerialMaker;

type
  TFrmVersaoteste = class(TForm)
    painel: TPanel;
    imgLogo: TImage;
    bottom: TPanel;
    btnFechar: TJvImgBtn;
    btnContinuar: TJvImgBtn;
    pbDias: TJvgProgress;
    btnAtivar: TJvImgBtn;
    imagens: TPngImageList;
    lblMensagem: TLabel;
    Autenticador: TJvSerialMaker;
    procedure btnContinuarClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnAtivarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure ReduzUso;
    function GetSerialVolDec(sSerHd : String): integer;
  private
    iCDU : integer;
    hCodAct,sPName,sPSerial : string;
  public
    { Public declarations }
  end;

var
  FrmVersaoteste: TFrmVersaoteste;

implementation

uses UFrmPrincipal, UFrmForAtiv;

{$R *.dfm}

function TFrmVersaoteste.GetSerialVolDec(sSerHd: String): integer;
begin
  try
    Result := StrToInt('$' + sSerHd);
  except
    on Exception do
      Result := 0;
  end;
end;

procedure TFrmVersaoteste.btnAtivarClick(Sender: TObject);
begin
  FrmForAtiv := TFrmForAtiv.Create(Self);
  FrmVersaoteste.Hide;
  FrmForAtiv.Show;
end;

procedure TFrmVersaoteste.btnContinuarClick(Sender: TObject);
begin
  ReduzUso;
  FreeAndNil(FrmVersaoteste);
  FrmPrincipal.Show;
end;

procedure TFrmVersaoteste.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmVersaoteste.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Application.Terminate;
end;

procedure TFrmVersaoteste.FormCreate(Sender: TObject);
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\WestSoftwares\ImgCom',true);
  if not padrao.ValueExists('ProductCDU') then
    padrao.WriteInteger('ProductCDU',3200);
  iCDU := padrao.ReadInteger('ProductCDU');
  hCodAct := padrao.ReadString('ProductCodAtiv');
  sPName := padrao.ReadString('ProductName');
  sPSerial := padrao.ReadString('ProductSerial');
  padrao.CloseKey();
  padrao.Free;
end;

procedure TFrmVersaoteste.FormShow(Sender: TObject);
begin
  lblMensagem.Caption := 'Esta � uma vers�o de testes. ' + #13
                      + 'Tipo de teste: Utiliza��o.' + #13
                      + 'Per�odo de teste: 100 Utiliza��es.' + #13
                      + 'Depois desse per�odo ser� necess�rio ativar!';
  pbDias.Percent := iCDU div 32;
  pbDias.Caption := 'Restam ' + inttostr(iCDU div 32) + ' Utiliza��es';
  if iCDU = 0 then
    begin
      btnContinuar.Enabled := False;
      btnAtivar.SetFocus;
    end
end;

procedure TFrmVersaoteste.ReduzUso;
var
  padrao: TRegistry;
begin
  padrao := TRegistry.Create;
  padrao.RootKey := HKEY_CURRENT_USER;
  padrao.OpenKey('Software\WestSoftwares\ImgCom',true);
  iCDU := iCDU - 32;
  padrao.WriteInteger('ProductCDU',iCDU);
  padrao.CloseKey();
  padrao.Free;
end;

end.
