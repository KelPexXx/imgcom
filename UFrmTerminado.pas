unit UFrmTerminado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, JvExStdCtrls, JvButton, JvCtrls, pngimage,ShellApi;

type
  TFrmTerminado = class(TForm)
    btnFechar: TJvImgBtn;
    btnAbrirPasta: TJvImgBtn;
    btnOk: TJvImgBtn;
    imgMsg: TImage;
    lblMsg: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure btnFecharClick(Sender: TObject);
    procedure btnAbrirPastaClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmTerminado: TFrmTerminado;

implementation

uses UFrmPrincipal;

{$R *.dfm}

procedure TFrmTerminado.btnAbrirPastaClick(Sender: TObject);
begin
  ShellExecute(Application.HANDLE, 'open', PChar(FrmPrincipal.lblLocalDestDir.Caption),nil,nil,SW_SHOWNORMAL);
  Close;
  FrmPrincipal.Close;
end;

procedure TFrmTerminado.btnFecharClick(Sender: TObject);
begin
  Close;
  FrmPrincipal.Close;
end;

procedure TFrmTerminado.btnOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
  Close;
end;

end.
