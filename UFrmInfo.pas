unit UFrmInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, pngimage, ExtCtrls;

type
  TFrmInfo = class(TForm)
    logo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmInfo: TFrmInfo;

implementation

{$R *.dfm}

procedure TFrmInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FreeAndNil(FrmInfo);
end;

end.
