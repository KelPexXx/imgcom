unit UFrmForAtiv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, JvComponentBase, JvSerialMaker, JvExStdCtrls,
  JvButton, JvCtrls, ImgList, PngImageList,IdSMTP,IdHTTP, IdMessage, IdBaseComponent,
  IdComponent, IdIOHandler, IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL,
  IDExplicitTlsClientServerBase;

type
  TFrmForAtiv = class(TForm)
    top: TPanel;
    botton: TPanel;
    edtCodAtivacao: TLabeledEdit;
    Autenticador: TJvSerialMaker;
    btnProximo: TJvImgBtn;
    btnFechar: TJvImgBtn;
    imagens: TPngImageList;
    btnVoltar: TJvImgBtn;
    lbltexto: TLabel;
    btnEnviarEmail: TJvImgBtn;
    edtNomeAtiv: TLabeledEdit;
    btnInfo: TJvImgBtn;
    edtEmail: TLabeledEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure btnFecharClick(Sender: TObject);
    procedure btnVoltarClick(Sender: TObject);
    procedure btnProximoClick(Sender: TObject);
    function GetSerialVolNum(const aDrive : Char) : string;
    procedure EnviarEmail(sNome,sCodAtiv,sEmail:string);
    procedure FormCreate(Sender: TObject);
    procedure btnInfoClick(Sender: TObject);
    procedure btnEnviarEmailClick(Sender: TObject);
  private
    hSerialHD:String;
  public
    { Public declarations }
  end;

var
  FrmForAtiv: TFrmForAtiv;

implementation

uses UFrmVersaoteste, UFrmAtivacao, UFrmInfo;

{$R *.dfm}

procedure TFrmForAtiv.btnEnviarEmailClick(Sender: TObject);
begin
  EnviarEmail(edtNomeAtiv.Text,edtCodAtivacao.Text,edtEmail.Text);
end;

procedure TFrmForAtiv.btnFecharClick(Sender: TObject);
begin
  Close;
  Application.Terminate;
end;

procedure TFrmForAtiv.btnInfoClick(Sender: TObject);
begin
  FrmInfo := TFrmInfo.Create(Self);
  FrmInfo.Show;
end;

procedure TFrmForAtiv.btnProximoClick(Sender: TObject);
begin
  FrmAtivacao := TFrmAtivacao.Create(Self);
  FrmForAtiv.Hide;
  FrmAtivacao.Show;
end;

procedure TFrmForAtiv.btnVoltarClick(Sender: TObject);
begin
  FrmVersaoteste.Show;
  FreeAndNil(FrmForAtiv);
  if Assigned(FrmAtivacao) then
    FreeAndNil(FrmAtivacao);
  if Assigned(FrmInfo) then
    FreeAndNil(FrmInfo);
end;

procedure TFrmForAtiv.EnviarEmail(sNome, sCodAtiv, sEmail:string);
var
  IdMessage: TIdMessage;
  IdSMTP   : TIdSMTP;
  IdSSLSocket: TIdSSLIOHandlerSocketOpenSSL;
begin
   try
     IdMessage                  := TIdMessage.Create( nil );
     IdSMTP                     := TIdSMTP.Create( nil );
     IdSSLSocket                := TIdSSLIOHandlerSocketOpenSSL.Create( nil );

     with IdSMTP do begin
      AuthType                  := satDefault;
      Host                      := 'smtp.gmail.com';
      Username                  := 'ws.ativacao@gmail.com';
      Password                  := '53RV3R@mmes';
      Port                      := 465;
      IOHandler                 := IdSSLSocket;
      UseTLS                    := utUseExplicitTLS;
     end;

     IdSSLSocket.SSLOptions.Method := sslvSSLv23;
     IdSSLSocket.SSLOptions.Mode := sslmClient;

     with IdMessage do begin
      From.Address              := 'ws.ativacao@gmail.com';
      From.Name                 := sNome;
      Recipients.EMailAddresses := 'ws.ativacao@gmail.com';
      Subject                   := sCodAtiv;
      Body.Text                 := 'ImgCom v2.0.5 : '+sNome+' : '+sCodAtiv+' : '+sEmail;
     end;

     IdSMTP.Connect;
     IdSMTP.Authenticate;
     try
      IdSMTP.Send( IdMessage );
      ShowMessage('Enviado com sucesso!');
     finally
      IdSMTP.Disconnect;
      if Assigned( IdMessage ) then
       FreeAndNil( IdMessage );
      if Assigned( IdSMTP ) then
       FreeAndNil( IdSMTP );
     end;
   except
   end;
end;

procedure TFrmForAtiv.FormCreate(Sender: TObject);
begin
  hSerialHD := GetSerialVolNum('C');
  edtCodAtivacao.Text := hSerialHD;
end;

function TFrmForAtiv.GetSerialVolNum(const aDrive: Char): string;
var
  Serial : DWord;
  DirLen , Flags: DWord;
  DLabel : Array[0..11] of Char;
begin
  try
    GetVolumeInformation(PChar(UpCase(aDrive) + ':\'),
      dLabel, 12, @Serial, DirLen, Flags, nil, 0);
    Result := IntToHex(Serial, 8);
  except
    Result := '';
  end;
end;

end.
